@extends('layouts.app')
@section('css')
    <link href="{{ asset('css/admin.css') }}" rel="stylesheet">
@endsection
@section('content')
<section class="home-head">
    <div class="container">
      <div class="row">
            <div class="col-sm-8">
              <div class="card">
                <div class="card-body">
                <h6 class="card-title">ผลการค้นหา: "{{$word}}"</h6>
                  <ul class="list-group list-group-flush">
                    @foreach($data as $item) 
                          <li class="list-group-item">
                              @if($item->application == 0)
                              <a href="/admin/program/{{$item->id}}" class="none-link">
                                <i class="icon-type fab fa-windows fa-xs"></i>
                                <img src="{{$item->icon}}"/>
                                <div class="prosub">{{$item->name}}</div>
                                <a href="/admin/programs/category/{{$item->category}}" class="none-link">
                                <p class="typePro"><i class="far fa-folder-open"> {{$item->category}}</i></p>
                                </a>
                                <p class="carPro"><i class="far fa-clock"> {{ date('d-M-Y', strtotime($item->created_at)) }} </i></p>
                                <p class="numPro"><i class="fas fa-download fa-xs"></i> {{$item->download}}</p>
                                <a href="/admin/program/{{$item->id}}" class="none-link">
                                    <p class="delPro"><i class="fas fa-trash"></i></p>
                                </a>
                            </a>
                              @elseif($item->application == 1)
                              <a href="/admin/application/{{$item->id}}" class="none-link">
                                <i class="icon-type fas fa-mobile fa-xs"></i>
                                <img src="{{$item->icon}}"/>
                                <div class="prosub">{{$item->name}}</div>
                                <a href="/admin/applications/category/{{$item->category}}" class="none-link">
                                <p class="typePro"><i class="far fa-folder-open"> {{$item->category}}</i></p>
                                </a>
                                <p class="carPro"><i class="far fa-clock"> {{ date('d-M-Y', strtotime($item->created_at)) }} </i></p>
                                <p class="numPro"><i class="fas fa-download fa-xs"></i> {{$item->download}}</p>
                                <a href="/admin/application/{{$item->id}}" class="none-link">
                                    <p class="delPro"><i class="fas fa-trash"></i></p>
                                </a>
                              @else
                              <a href="/admin/game/{{$item->id}}" class="none-link">
                                <i class="icon-type fas fa-gamepad fa-xs"></i>
                                <img src="{{$item->icon}}"/>
                                <div class="prosub">{{$item->name}}</div>
                                <a href="/admin/games/category/{{$item->category}}" class="none-link">
                                <p class="typePro"><i class="far fa-folder-open"> {{$item->category}}</i></p>
                                </a>
                                <p class="carPro"><i class="far fa-clock"> {{ date('d-M-Y', strtotime($item->created_at)) }} </i></p>
                                <p class="numPro"><i class="fas fa-download fa-xs"></i> {{$item->download}}</p>
                                <a href="/admin/game/{{$item->id}}" class="none-link">
                                    <p class="delPro"><i class="fas fa-trash"></i></p>
                                </a>
                              @endif
                        </li>
                    @endforeach
                  </ul>
                </div>
              </div>
              <div class="pagination">{{$data->links()}}</div>
            </div>
            <div class="col-sm-4 side-bar">
              <form class="form-inline" action="/admin/search" method="GET">
                <div class="input-group mb-3" style="width: 100%">
                    <input type="text" class="form-control" name="word" placeholder="ค้นหาข้อมูล" aria-label="ค้นหาข้อมูล" aria-describedby="basic-addon2">
                    <div class="input-group-append">
                    <button class="btn btn-secondary" type="submit">ค้นหา</button>
                    </div>
                </div>
            </form>
        </div>
      </div>
  </section>
@endsection
    