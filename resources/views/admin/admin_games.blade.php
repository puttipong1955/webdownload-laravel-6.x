@extends('layouts.app')
@section('css')
    <link href="{{ asset('css/admin.css') }}" rel="stylesheet">
@endsection
@section('js')
    <script src="http://code.jquery.com/jquery-3.3.1.min.js"
    integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
    crossorigin="anonymous">
    </script>
@endsection
@section('end-page')
    <script>
    $(document).on('click','.open_modal',function(event){
      var url = "/admin/game/get";
      var tour_id= $(this).attr('id');
      $.get(url + '/' + tour_id, function (data) {
          //success data
          console.log(data);
          $('#id').val(data.id);
          $('#name').val(data.name);
          $('#text').val(data.info);
          $('#company').val(data.company);
          $('#tag').val(data.tag);
          $('#category').val(data.category);
          $('#os').val(data.os);
          $('#date_out').val(data.date_out);
          $('#size').val(data.size);
          $('#cpu').val(data.cpu);
          $('#disk').val(data.disk);
          $('#ram').val(data.ram);
          $('#grafig').val(data.grafig);
          $('#video').val(data.video);
          $('#icon').val(data.icon);
          $('#pic').val(data.pic);
          $('#pic2').val(data.pic2);
          $('#pic3').val(data.pic3);
          $('#pic4').val(data.pic4);
          $('#btn').html("Update");
          $('#form-set').attr('action', "update-game");
          $('#modalForm').modal('show');
      }) 
    });  

    $(document).on('click','.open_Linksmodal',function(event){
      $("#gameLinksList").empty();
      var url = "/admin/game-links/get";
      var tour_id= $(this).attr('id');
      $.get(url + '/' + tour_id, function (gameLinks) {
          //success data
          console.log(gameLinks);
            jQuery.each(gameLinks[0], function(index, value){
              var type = value['type'];
              if(type == 0)
              $("#gameLinksList").append("<li id="+ value['id'] +" class='list-group-item modal-link'><b>แบ่งพาร์ท</b>  Part" + value['part'] + ":  " + value['link'] 
              + "<a href='/admin/game-link/del/"+ value['id'] +"' style='color: black'><i class='fas fa-trash' style='right: 0px; position: absolute;'></i></a></li>");
              else if(type == 1)
              $("#gameLinksList").append("<li id="+ value['id'] +" class='list-group-item modal-link'><b>ไฟล์เดียว</b> : " + value['link'] 
              + "<a href='/admin/game-link/del/"+ value['id'] +"' style='color: black'><i class='fas fa-trash' style='right: 0px; position: absolute;'></i></a></li>");
              else if(type == 2)
              $("#gameLinksList").append("<li id="+ value['id'] +" class='list-group-item modal-link'><b>Crack</b> : " + value['link']
               + "<a href='/admin/game-link/del/"+ value['id'] +"' style='color: black'><i class='fas fa-trash' style='right: 0px; position: absolute;'></i></a></li>");
              else $("#gameLinksList").append("<li id="+ value['id'] +" class='list-group-item modal-link'><b>Patch</b> : " + value['link'] 
              + "<a href='/admin/game-link/del/"+ value['id'] +"' style='color: black'><i class='fas fa-trash' style='right: 0px; position: absolute;'></i></a></li>");
              
            });
            
          $('#btn-add').val(gameLinks[1]);
          $('#modalLinks').modal('show');
      }) 
    });  

    $(document).on('click','.modal-link',function(event){
      var url = "/admin/game-link/get";
      var tour_id= $(this).attr('id');
      $.get(url + '/' + tour_id, function (gameLink) {
          //success data
          console.log(gameLink);
          $('#id-link').val(gameLink.id);
          $('#game-id').val(gameLink.id_game);
          $('#form-game-link').val(gameLink.link);
          $('#form-game-part').val(gameLink.part);
          $('#form-game-type').val(gameLink.type);
          $('#modalLinkForm').modal('show');
      }) 
    });  

    $(document).on('click','.modal-add-link',function(event){
        var game_id= $(this).attr('value');
          $('#game-id').val(game_id);
          $('#form-game-link').val("");
          $('#form-game-part').val("");
          $('#form-game-type').val("");
          $('#btn-link').html("Add");
          $('#label-link').html("Add Link");
          $('#form-link').attr('action', "submit-game-link");
          $('#modalLinkForm').modal('show');
    });  
    
    $(document).on('click','.clear_modal',function(event){
          $('#name').val("");
          $('#text').val("");
          $('#company').val("");
          $('#tag').val("");
          $('#category').val("");
          $('#os').val("");
          $('#date_out').val("");
          $('#size').val("");
          $('#cpu').val("");
          $('#disk').val("");
          $('#ram').val("");
          $('#grafig').val("");
          $('#video').val("");
          $('#icon').val("");
          $('#pic').val("");
          $('#pic2').val("");
          $('#pic3').val("");
          $('#pic4').val("");
          $('#btn').html("Post");
          $('#form-set').attr('action', "submit-game");
          $('#method').remove();
    });  
    </script>
@endsection 
@section('content')
<section class="home-head">
<!-- Modal Insert-->
<div id="modalForm" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div style="padding: 10px">
      <form id="form-set" action="submit-game" method="POST">
        @csrf
        <input type="hidden" name="id" id="id" value=""/>
        <div class="form-group">
          <label for="inputAddress">Name</label>
          <input type="text" class="form-control" id="name" name="name" placeholder="ควรใส่คีเวิร์ดหน้าๆ ชื่อไม่ควรซ้ำ สั้นๆ" required>
        </div>
        <div class="form-group">
          <label for="exampleFormControlTextarea1">Text</label>
          <textarea class="form-control" id="text" name="text" rows="7" required placeholder="ใส่เนื้อหาที่เขียนเอง ควรใส่คีเวิร์ด 2-3 คำ เกี่ยวข้องกับบทความ และต้องยาวพอสมควร"></textarea>
        </div>
        <div class="form-group">
          <label for="inputAddress">Tag</label>
          <input type="text" class="form-control" id="tag" name="tag" placeholder="ควรใส่คำที่เป็นประโยชน์เกี่ยวข้องกับบทความ (คั่นด้วย,ไม่ต้องวรรค)" required>
        </div>
        <hr>
        <div class="form-row">
          <div class="form-group col-md-4">
            <label for="inputZip">Category</label>
            <input type="text" class="form-control" id="category" name="category" placeholder="หมวดหมู่" required>
          </div>
          <div class="form-group col-md-4">
            <label for="inputZip">Company</label>
            <input type="text" class="form-control" id="company" name="company" placeholder="บริษัท">
          </div>
          <div class="form-group col-md-4">
            <label for="inputZip">Date-Sale</label>
            <input type="date" class="form-control" id="date_out" name="date_out" placeholder="วันที่ออกจำหน่าย (2020-01-18)">
          </div>
        </div>
        <div class="form-row">
          <div class="form-group col-md-3">
            <label for="inputZip">OS</label>
            <input type="text" class="form-control" id="os" name="os" placeholder="ระบบปฏิบัติการ" maxlength="20">
          </div>
          <div class="form-group col-md-3">
            <label for="inputZip">Size</label>
            <input type="text" class="form-control" id="size" name="size" placeholder="ขนาดไฟล์" maxlength="20">
          </div>
        </div>
            <div class="form-row">
              <div class="form-group col-md-3">
                <label for="inputZip">CPU</label>
                <input type="text" class="form-control" id="cpu" name="cpu" placeholder="CPU" maxlength="20">
              </div>
              <div class="form-group col-md-3">
                <label for="inputZip">Ram</label>
                <input type="text" class="form-control" id="ram" name="ram" placeholder="Ram" maxlength="20">
              </div>
              <div class="form-group col-md-3">
                <label for="inputZip">GPU</label>
                <input type="text" class="form-control" id="grafig" name="grafig" placeholder="การ์จอ" maxlength="100">
              </div>
              <div class="form-group col-md-3">
                <label for="inputZip">HDD</label>
                <input type="text" class="form-control" id="disk" name="disk" placeholder="ฮาร์ดดิส" maxlength="30">
              </div>
        </div>
        <hr>
        <div class="form-group">
          <label for="inputAddress2">Video</label>
          <input type="text" class="form-control" id="video" name="video" placeholder="ลิ้งค์วิดีโอ Youtube(oVQ0YLcMv6U)" required>
        </div>
        <div class="form-group">
          <label for="inputAddress2">Icon</label>
          <input type="text" class="form-control" id="icon" name="icon" placeholder="ลิ้งค์รูปภาพปก" required>
        </div>
        <div class="form-group">
          <label for="inputAddress2">Pic</label>
          <input type="text" class="form-control" id="pic" name="pic" placeholder="ลิ้งค์รูปภาพที่ 1" required>
        </div>
        <div class="form-group">
          <label for="inputAddress2">Pic2</label>
          <input type="text" class="form-control" id="pic2" name="pic2" placeholder="ลิ้งค์รูปภาพที่ 2">
        </div>
        <div class="form-group">
          <label for="inputAddress2">Pic3</label>
          <input type="text" class="form-control" id="pic3" name="pic3" placeholder="ลิ้งค์รูปภาพที่ 3">
        </div>
        <div class="form-group">
          <label for="inputAddress2">Pic4</label>
          <input type="text" class="form-control" id="pic4" name="pic4" placeholder="ลิ้งค์รูปภาพที่ 4">
        </div>
        
        <button id="btn" style="margin-left: 90%" type="submit" class="btn btn-primary">Post</button>
      </form>
    </div>
    </div>
  </div>
</div>

<div id="modalLinks" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div style="padding: 10px">
        <div class="form-group">
          <label for="inputAddress2">Links</label>
          <ul id="gameLinksList" class="list-group list-group-flush">

          </ul>
        </div>
        <button id="btn-add" style="margin-left: 90%" type="submit" value="" class="btn btn-primary modal-add-link">Add</button>
    </div>
    </div>
  </div>
</div>

<div id="modalLinkForm" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div style="padding: 10px">
      <form id="form-link" action="update-game-link" method="POST">
        @csrf
        <div class="form-group">
          <label id="label-link" for="inputAddress2">Update Link</label>
          <div class="form-row">
            <input type="hidden" id="id-link" name="id-link" value=""/>
            <input type="hidden" id="game-id" name="game-id" value=""/>
            <div class="form-group col-md-6">
              <label for="inputZip">Link</label>
              <input type="text" class="form-control" id="form-game-link" name="form-game-link" placeholder="ลิ้งค์" required>
            </div>
            <div class="form-group col-md-3">
              <label for="inputZip">Part</label>
              <input type="text" class="form-control" id="form-game-part" name="form-game-part" placeholder="ลำดับ (1,2,3)" required> 
            </div>
            <div class="form-group col-md-3">
              <label for="inputZip">Type</label>
              <select id="form-game-type" name="form-game-type" class="form-control" required>
                <option value="0">แบ่งพาร์ท</option>
                <option value="1">ไฟล์เดียว</option>
                <option value="2">Crack</option>
                <option value="3">Patch</option>
              </select>
            </div>
          </div>
          <button id="btn-link" style="margin-left: 90%" type="submit" class="btn btn-primary">Update</button>
        </div>
        </form>
    </div>
    </div>
  </div>
</div>
<!-- End-->

    <div class="container">
      <div class="row">
            <div class="col-sm-8">
              <a href="#" class="clear_modal" style="color: black" data-toggle="modal" data-target=".bd-example-modal-lg">
                <div class="card create"><i class="fas fa-pen"></i></div>
              </a>
              <div class="card">
                <div class="card-body">
                    <h6 class="card-title">เกมส์ล่าสุด</h6>
                    <p class="article-num">จำนวน: {{count($gamesLast)}} บทความ</p>
                  <ul class="list-group list-group-flush">
                    @foreach($gamesLast as $item) 
                          <li class="list-group-item">
                            <a href="#" id="{{$item->id}}" class="none-link open_modal">
                                <p class="id-game">{{$item->id}}</p>
                                <img src="{{$item->icon}}"/>
                                <div class="prosub">{{$item->name}}</div>
                                <a href="/admin/games/category/{{$item->category}}" class="none-link">
                                    <p class="typePro"><i class="far fa-folder-open"> {{$item->category}}</i></p>
                                </a>
                                <p class="Linklist open_Linksmodal" id="{{$item->id}}"><i class="fa fa-link" aria-hidden="true"></i></p>
                                <p class="carPro"><i class="far fa-clock"> {{ date('d-M-Y', strtotime($item->created_at)) }} </i></p>
                                <a href="/game/{{$item->slug}}" target="_blank" class="none-link">
                                  <p class="desktop"><i class="fas fa-desktop"></i></p>
                                </a>
                                <p class="numPro"><i class="fas fa-download fa-xs"></i> {{$item->view}}</p>
                                <a href="/admin/2/{{$item->id}}" onclick="return confirm('Are you sure?')" class="none-link">
                                  <p class="delPro"><i class="fas fa-trash"></i></p>
                              </a>
                            </a>
                        </li>
                    @endforeach
                  </ul>
                </div>
              </div>
              <div class="pagination">{{$gamesLast->links()}}</div>
            </div>
            <div class="col-sm-4 side-bar">
                <form class="form-inline" action="/admin/search" method="GET">
                    <div class="input-group mb-3" style="width: 100%">
                        <input type="text" class="form-control" name="word" placeholder="ค้นหาข้อมูล" aria-label="ค้นหาข้อมูล" aria-describedby="basic-addon2">
                        <div class="input-group-append">
                        <button class="btn btn-secondary" type="submit">ค้นหา</button>
                        </div>
                    </div>
                </form>
                <div class="card">
                  <div class="card-body">
                      <h6 class="card-title">หมวดหมู่</h6>
                        <div class="category">
                            <p>
                                @foreach($categories as $item) 
                                    <a href="/admin/games/category/{{$item->category}}">{{$item->category}}, </a>
                                 @endforeach
                            <p>
                        </div>
                  </div>
                </div>
              <div class="card card-hit">
                <div class="card-body">
                    <h6 class="card-title">เกมส์ยอดนิยม</h6>
                  <ul class="list-group list-group-flush">
                    @foreach($gamesHit as $item) 
                          <li class="list-group-item">
                            
                                <img src="{{$item->icon}}"/>
                                <div class="prosub-side">{{$item->name}}</div>
                                <p class="carPro-side"><i class="far fa-clock"> {{ date('d-M-Y', strtotime($item->created_at)) }} </i></p>
                                <p class="numPro-side"><i class="fas fa-download fa-xs"></i> <sub>{{$item->view}}</sub></p>
                            
                        </li>
                    @endforeach
                  </ul>
                </div>
              </div>
              
            </div>
          </div>
        </div>
      </div>
  </section>
@endsection
    