@extends('layouts.app')

@section('css')
    <link href="{{ asset('css/admin.css') }}" rel="stylesheet">
@endsection
@section('content')
<section class="count">
    <?php $sumPro = 0; $sumApp = 0; $sumGame = 0;?>
    @foreach($programViews as $item) 
        {{$sumPro += $item['download']}}
    @endforeach
    @foreach($appViews as $item) 
        {{$sumApp += $item['download']}}
    @endforeach
    @foreach($gameViews as $item) 
        {{$sumGame += $item['view']}}
    @endforeach
</section>
<section class="home-head">
  <div class="container">
    <div class="row">
        <div class="col-sm-4">
          <div class="card">
            <div class="card-body">
              <a href="/admin/programs"><button  class="btn btn-primary"><i class="fab fa-windows fa-3x"></i></button></a>
              <h5 class="card-title">Programs</h5>
              <p class="card-text">{{$sumPro}} <i class="load fas fa-download fa-xs"></i></p>
            </div>
          </div>
        </div>
        <div class="col-sm-4">
            <div class="card">
              <div class="card-body">
                <a href="/admin/applications"><button class="btn btn-success"><i class="fas fa-mobile fa-3x"></i></button></a>
                <h5 class="card-title">Applications</h5>
                <p class="card-text">{{$sumApp}} <i class="load fas fa-download fa-xs"></i></p>
              </div>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="card">
              <div class="card-body">
                <a href="/admin/games"><button class="btn btn-warning"><i class="fas fa-gamepad fa-3x"></i></button></a>
                <h5 class="card-title">Games</h5>
                <p class="card-text">{{$sumGame}} <i class="load fas fa-download fa-xs"></i></p>
              </div>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="card">
              <div class="card-body">
                <ul class="list-group list-group-flush">
                    @foreach($programsHit as $item) 
                        <li class="list-group-item">
                                <img src="{{$item->icon}}"/>
                                <div class="isub">{{$item->name}}</div>
                                <p class="num">{{$item->download}} <i class="fas fa-download fa-xs"></i></p>
                            </li>
                    @endforeach
                </ul>
              </div>
            </div>
            <a href="/admin/programs"><button class="btn btn-primary more">More <i class="fas fa-angle-right"></i></button></a>
          </div>
          <div class="col-sm-4">
            <div class="card">
              <div class="card-body">
                <ul class="list-group list-group-flush">
                    @foreach($applicationsHit as $item) 
                        <li class="list-group-item">
                                <img src="{{$item->icon}}"/>
                                <div class="isub">{{$item->name}}</div>
                                <p class="num">{{$item->download}} <i class="fas fa-download fa-xs"></i></p>
                          </li>
                    @endforeach
                </ul>
              </div>
            </div>
            <a href="/admin/applications"><button class="btn btn-success more">More <i class="fas fa-angle-right"></i></button></a>
          </div>
          <div class="col-sm-4">
            <div class="card">
              <div class="card-body">
                <ul class="list-group list-group-flush">
                    @foreach($gamesHit as $item) 
                            <li class="list-group-item">
                                    <img src="{{$item->icon}}"/>
                                    <div class="isub">{{$item->name}}</div>
                                    <p class="num">{{$item->view}} <i class="fas fa-download fa-xs"></i></p>
                            </li>
                    @endforeach
                </ul>
              </div>
            </div>
            <a href="/admin/games"><button class="btn btn-warning more">More <i class="fas fa-angle-right"></i></button></a>
          </div>
        </div>
    </div>
</section>
@endsection
    