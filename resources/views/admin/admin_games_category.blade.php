@extends('layouts.app')
@section('css')
    <link href="{{ asset('css/admin.css') }}" rel="stylesheet">
@endsection
@section('content')
<section class="home-head">
    <div class="container">
      <div class="row">
            <div class="col-sm-8">
              <div class="card">
                <div class="card-body">
                  <h6 class="card-title">เกมส์หมวดหมู่: {{$category}}</h6>
                  <ul class="list-group list-group-flush">
                    @foreach($gamesCategory as $item) 
                          <li class="list-group-item">
                            <a href="/admin/program/{{$item->id}}" class="none-link">
                                <img src="{{$item->icon}}"/>
                                <div class="prosub">{{$item->name}}</div>
                                <a href="/admin/programs/category/{{$item->category}}" class="none-link">
                                    <p class="typePro"><i class="far fa-folder-open"> {{$item->category}}</i></p>
                                </a>
                                <p class="carPro"><i class="far fa-clock"> {{ date('d-M-Y', strtotime($item->created_at)) }} </i></p>
                                <p class="numPro"><i class="fas fa-download fa-xs"></i> {{$item->view}}</p>
                                <a href="/admin/program/{{$item->id}}" class="none-link">
                                    <p class="delPro"><i class="fas fa-trash"></i></p>
                                </a>
                            </a>
                        </li>
                    @endforeach
                  </ul>
                </div>
              </div>
              <div class="pagination">{{$gamesCategory->links()}}</div>
            </div>
            <div class="col-sm-4 side-bar">
                <form class="form-inline" action="/admin/search" method="GET">
                  <div class="input-group mb-3" style="width: 100%">
                      <input type="text" class="form-control" name="word" placeholder="ค้นหาข้อมูล" aria-label="ค้นหาข้อมูล" aria-describedby="basic-addon2">
                      <div class="input-group-append">
                      <button class="btn btn-secondary" type="submit">ค้นหา</button>
                      </div>
                  </div>
              </form>
                <div class="card">
                  <div class="card-body">
                      <h6 class="card-title">หมวดหมู่</h6>
                        <div class="category">
                            <p>
                                @foreach($categories as $item) 
                                    <a href="/admin/games/category/{{$item->category}}">{{$item->category}}, </a>
                                 @endforeach
                            <p>
                        </div>
                  </div>
                </div>
                <div class="card card-hit">
                    <div class="card-body">
                        <h6 class="card-title">เกมส์ยอดนิยม</h6>
                      <ul class="list-group list-group-flush">
                        @foreach($gamesHit as $item) 
                              <li class="list-group-item">
                                <a href="/admin/program/{{$item->id}}" class="none-link">
                                    <img src="{{$item->icon}}"/>
                                    <div class="prosub-side">{{$item->name}}</div>
                                    <p class="carPro-side"><i class="far fa-clock"> {{ date('d-M-Y', strtotime($item->created_at)) }} </i></p>
                                    <p class="numPro-side"><i class="fas fa-download fa-xs"></i> <sub>{{$item->view}}</sub></p>
                                </a>
                            </li>
                        @endforeach
                      </ul>
                    </div>
                  </div>
              </div>
        </div>
      </div>
  </section>
@endsection
    