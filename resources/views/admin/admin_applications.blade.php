@extends('layouts.app')
@section('css')
    <link href="{{ asset('css/admin.css') }}" rel="stylesheet">
@endsection
@section('js')
    <script src="http://code.jquery.com/jquery-3.3.1.min.js"
    integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
    crossorigin="anonymous">
    </script>
@endsection
@section('end-page')
    <script>
    $(document).on('click','.open_modal',function(event){
      var url = "/admin/program/get";
      var tour_id= $(this).attr('id');
      $.get(url + '/' + tour_id, function (data) {
          //success data
          console.log(data);
          $('#id').val(data.id);
          $('#name').val(data.name);
          $('#text').val(data.text);
          $('#news').val(data.news);
          $('#tag').val(data.tag);
          $('#category').val(data.category);
          $('#os').val(data.os);
          $('#copyright').val(data.copyright);
          $('#size').val(data.size);
          $('#version').val(data.version);
          $('#link').val(data.link);
          $('#icon').val(data.icon);
          $('#pic').val(data.pic);
          $('#pic2').val(data.pic2);
          $('#pic3').val(data.pic3);
          $('#btn').html("Update");
          $('#form-set').attr('action', "update-application");
          $('#modalForm').modal('show');
      }) 
    });  
    
    $(document).on('click','.clear_modal',function(event){
          $('#name').val("");
          $('#text').val("");
          $('#news').val("");
          $('#tag').val("");
          $('#category').val("");
          $('#os').val("");
          $('#copyright').val("");
          $('#size').val("");
          $('#version').val("");
          $('#link').val("");
          $('#icon').val("");
          $('#pic').val("");
          $('#pic2').val("");
          $('#pic3').val("");
          $('#btn').html("Post");
          $('#form-set').attr('action', "submit-application");
          $('#method').remove();
    });  
    </script>
@endsection 
@section('content')
<section class="home-head">
  <!-- Modal Insert-->
<div id="modalForm" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div style="padding: 10px">
      <form id="form-set" action="submit-application" method="POST">
        @csrf
        <input type="hidden" name="id" id="id" value=""/>
        <div class="form-group">
          <label for="inputAddress">Name</label>
          <input type="text" class="form-control" id="name" name="name" placeholder="ควรใส่คีเวิร์ดหน้าๆ ชื่อไม่ควรซ้ำ สั้นๆ" required>
        </div>
        <div class="form-group">
          <label for="exampleFormControlTextarea1">Text</label>
          <textarea class="form-control" id="text" name="text" rows="5" required placeholder="ใส่เนื้อหาที่เขียนเอง ควรใส่คีเวิร์ด 2-3 คำ เกี่ยวข้องกับบทความ และต้องยาวพอสมควร"></textarea>
        </div>
        <div class="form-group">
          <label for="exampleFormControlTextarea1">What new</label>
          <textarea class="form-control" id="news" name="news" rows="3" placeholder="ใส่เนื้อหาที่เขียนเอง"></textarea>
        </div>
        <div class="form-group">
          <label for="inputAddress">Tag</label>
          <input type="text" class="form-control" id="tag" name="tag" placeholder="ควรใส่คำที่เป็นประโยชน์เกี่ยวข้องกับบทความ (คั่นด้วย,ไม่ต้องวรรค)" required>
        </div>
        <div class="form-row">
          <div class="form-group col-md-4">
            <label for="inputZip">Category</label>
            <input type="text" class="form-control" id="category" name="category" placeholder="หมวดหมู่" required>
          </div>
          <div class="form-group col-md-4">
            <label for="inputZip">OS</label>
            <input type="text" class="form-control" id="os" name="os" placeholder="ระบบปฏิบัติการ">
          </div>
          <div class="form-group col-md-4">
            <label for="inputZip">Copyright</label>
            <input type="text" class="form-control" id="copyright" name="copyright" placeholder="ลิขสิทธิ์">
          </div>
        </div>
        <div class="form-row">
          <div class="form-group col-md-2">
            <label for="inputZip">Size</label>
            <input type="text" class="form-control" id="size" name="size" placeholder="ขนาดไฟล์" maxlength="10">
          </div>
          <div class="form-group col-md-2">
            <label for="inputZip">Version</label>
            <input type="text" class="form-control" id="version" name="version" placeholder="เวอร์ชั่น">
          </div>
        </div>
        <div class="form-group">
          <label for="inputAddress2">Link</label>
          <input type="text" class="form-control" id="link" name="link" placeholder="ลิ้งค์ดาวน์โหลด" required>
        </div>
        <div class="form-group">
          <label for="inputAddress2">Icon</label>
          <input type="text" class="form-control" id="icon" name="icon" placeholder="ลิ้งค์รูปภาพปก" required>
        </div>
        <div class="form-group">
          <label for="inputAddress2">Pic</label>
          <input type="text" class="form-control" id="pic" name="pic" placeholder="ลิ้งค์รูปภาพที่ 1" required>
        </div>
        <div class="form-group">
          <label for="inputAddress2">Pic2</label>
          <input type="text" class="form-control" id="pic2" name="pic2" placeholder="ลิ้งค์รูปภาพที่ 2">
        </div>
        <div class="form-group">
          <label for="inputAddress2">Pic3</label>
          <input type="text" class="form-control" id="pic3" name="pic3" placeholder="ลิ้งค์รูปภาพที่ 3">
        </div>
        
        <button id="btn" style="margin-left: 90%" type="submit" class="btn btn-primary">Post</button>
      </form>
    </div>
    </div>
  </div>
</div>
<!-- End-->

    <div class="container">
      <div class="row">
            <div class="col-sm-8">
              <a href="#" class="clear_modal" style="color: black" data-toggle="modal" data-target=".bd-example-modal-lg">
                <div class="card create"><i class="fas fa-pen"></i></div>
              </a>
              <div class="card">
                <div class="card-body">
                    <h6 class="card-title">แอพพลิเคชั่นล่าสุด</h6>
                    <p class="article-num">จำนวน: {{count($applicationsLast)}} บทความ</p>
                  <ul class="list-group list-group-flush">
                    @foreach($applicationsLast as $item) 
                          <li class="list-group-item">
                            <a href="#" id="{{$item->id}}" class="none-link open_modal">
                                <p class="id-game">{{$item->id}}</p>
                                <img src="{{$item->icon}}"/>
                                <div class="prosub">{{$item->name}}</div>
                                <a href="/admin/applications/category/{{$item->category}}" class="none-link">
                                    <p class="typePro"><i class="far fa-folder-open"> {{$item->category}}</i></p>
                                </a>
                                <p class="carPro"><i class="far fa-clock"> {{ date('d-M-Y', strtotime($item->created_at)) }} </i></p>
                                <a href="/application/{{$item->slug}}" target="_blank" class="none-link">
                                  <p class="desktop"><i class="fas fa-desktop"></i></p>
                                </a>
                                <p class="numPro"><i class="fas fa-download fa-xs"></i> {{$item->download}}</p>
                                <a href="/admin/{{$item->application}}/{{$item->id}}" onclick="return confirm('Are you sure?')" class="none-link">
                                    <p class="delPro"><i class="fas fa-trash"></i></p>
                                </a>
                            </a>
                        </li>
                    @endforeach
                  </ul>
                </div>
              </div>
              <div class="pagination">{{$applicationsLast->links()}}</div>
            </div>
            <div class="col-sm-4 side-bar">
                <form class="form-inline" action="/admin/search" method="GET">
                    <div class="input-group mb-3" style="width: 100%">
                        <input type="text" class="form-control" name="word" placeholder="ค้นหาข้อมูล" aria-label="ค้นหาข้อมูล" aria-describedby="basic-addon2">
                        <div class="input-group-append">
                        <button class="btn btn-secondary" type="submit">ค้นหา</button>
                        </div>
                    </div>
                </form>
                <div class="card">
                  <div class="card-body">
                      <h6 class="card-title">หมวดหมู่</h6>
                        <div class="category">
                            <p>
                                @foreach($categories as $item) 
                                    <a href="/admin/applications/category/{{$item->category}}">{{$item->category}}, </a>
                                 @endforeach
                            <p>
                        </div>
                  </div>
                </div>
              <div class="card card-hit">
                <div class="card-body">
                    <h6 class="card-title">แอพพลเคชั่นยอดนิยม</h6>
                  <ul class="list-group list-group-flush">
                    @foreach($applicationsHit as $item) 
                          <li class="list-group-item">
                            
                                <img src="{{$item->icon}}"/>
                                <div class="prosub-side">{{$item->name}}</div>
                                <p class="carPro-side"><i class="far fa-clock"> {{ date('d-M-Y', strtotime($item->created_at)) }} </i></p>
                                <p class="numPro-side"><i class="fas fa-download fa-xs"></i> <sub>{{$item->download}}</sub></p>
                            
                        </li>
                    @endforeach
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
  </section>
@endsection
    