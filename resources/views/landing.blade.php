@extends('layouts/template')
@section('title', 'โหลดโปรแกรมฟรี ตัวเต็ม ถาวร โหลดเกมส์ pc โหลดแอพฟรี โหลดแอพ โหลดแอพเสียตังฟรี')
@section('description', 'โหลดโปรแกรมตัวเต็ม โหลดเกมส์pc ฟรี IDM ถาวร โหลดเกมส์ไฟล์เดียว เกมส์ตัวเต็ม โปรแกรมตัดต่อวิดีโอ โปรแกรมแต่งรูป โปรแกรมเอกสาร โปรแกรมดูหนัง โหลดแอพแต่งรูปฟรี แอพแอนดรอยด์ฟรี แอพไอโฟนฟรี windows 10 windows 7 windows 8.1 macos')
@section('css')
<link rel="stylesheet" href="css/style.css">
@endsection
@section('content')
<section class="section-head">
  <div class="container">
    <div class="row">
      <div class="col col-md-3">
        <div class="list-items">
          <h1 class="label-group">โปรแกรมฟรียอดนิยม</h1>
          <div class="list-group">
            @foreach ($programsHit as $program)
            <?php
              $string = $program->text;
              $replaced = str_replace(['&emsp;','<p>','</p>','<strong>','</strong>','&ensp;','&nbsp;','<b>','<i>','<u>','<h1>','<h2>','<h3>','</b>','</i>','</u>','</h1>','</h2>','</h3>','<br>','</br>'], "", $string);
            ?>
            <a href="/program/{{$program->slug}}" class="list-group-item list-group-item-action">
              <div class="list-content">
                <div class="list-image">
                  <img src="{{$program->icon}}" />
                </div>
                <div class="list-text">
                  <span>{{$program->name}}</span>
                  <p>{{$replaced}}</p>
                </div>
              </div>
            </a>
            @endforeach
          </div>
          <a href="/programs">
            <button class="btn-more btn btn-outline-primary">
              เพิ่มเติม
            </button>
          </a>

        </div>
      </div>
      <div class="col col-md-3">
        <div class="list-items">
          <h1 class="label-group">โปรแกรมตัวเต็มยอดนิยม</h1>
          <div class="list-group">
            @foreach ($programsFullHit as $program)
            <?php
              $string = $program->text;
              $replaced = str_replace(['&emsp;','<p>','</p>','<strong>','</strong>','&ensp;','&nbsp;','<b>','<i>','<u>','<h1>','<h2>','<h3>','</b>','</i>','</u>','</h1>','</h2>','</h3>','<br>','</br>'], "", $string);
            ?>
            <a href="/program/{{$program->slug}}" class="list-group-item list-group-item-action">
              <div class="list-content">
                <div class="list-image" alt="Line">
                  <img src="{{$program->icon}}" />
                </div>
                <div class="list-text">
                  <span>{{$program->name}}</span>
                  <p>{{$replaced}}</p>
                </div>
              </div>
            </a>
            @endforeach
          </div>
          <a href="/programs">
            <button class="btn-more btn btn-outline-primary">
              เพิ่มเติม
            </button>
          </a>
        </div>
      </div>
      <div class="col col-md-3">
        <div class="list-items">
          <h1 class="label-group">แอพพลิเคชั่นยอดนิยม</h1>
          <div class="list-group">
            @foreach ($applicationsHit as $application)
            <?php
              $string = $application->text;
              $replaced = str_replace(['&emsp;','<p>','</p>','<strong>','</strong>','&ensp;','&nbsp;','<b>','<i>','<u>','<h1>','<h2>','<h3>','</b>','</i>','</u>','</h1>','</h2>','</h3>','<br>','</br>'], "", $string);
            ?>
            <a href="/application/{{$application->slug}}" class="list-group-item list-group-item-action">
              <div class="list-content">
                <div class="list-image" alt="Line">
                  <img src="{{$application->icon}}" />
                </div>
                <div class="list-text">
                  <span>{{$application->name}}</span>
                  <p>{{$replaced}}</p>
                </div>
              </div>
            </a>
            @endforeach
          </div>
          <a href="/applications">
            <button class="btn-more btn btn-outline-primary">
              เพิ่มเติม
            </button>
          </a>
        </div>
      </div>
      <div class="col col-md-3">
        <div class="list-items">
          <h1 class="label-group">เกมส์ยอดนิยม</h1>
          <div class="list-group">
            @foreach ($gamesHit5 as $game)
            <?php
              $string = $game->info;
              $replaced = str_replace(['&emsp;','<p>','</p>','<strong>','</strong>','&ensp;','&nbsp;','<b>','<i>','<u>','<h1>','<h2>','<h3>','</b>','</i>','</u>','</h1>','</h2>','</h3>','<br>','</br>'], "", $string);
            ?>
            <a href="/game/{{$game->slug}}" class="list-group-item list-group-item-action">
              <div class="list-content">
                <div class="list-image" alt="Line">
                  <img src="{{$game->icon}}" />
                </div>
                <div class="list-text">
                  <span>{{$game->name}}</span>
                  <p>{{$replaced}}</p>
                </div>
              </div>
            </a>
            @endforeach
          </div>
          <a href="/games">
            <button class="btn-more btn btn-outline-primary">
              เพิ่มเติม
            </button>
          </a>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="section-pro-app">
  <div class="container-lg">
    <div class="row">
      <div class="col-md-2 cont1">
        <h2 class="cetagories-label">หมวดหมู่โปรแกรม</h2>
        <div class="list-group cetagories-list">
          @foreach ($categoriesPrograms as $catPro)
          <a href="/programs/category/{{$catPro->category}}" class="list-group-item list-group-item-action list-group-item-light">
            {{$catPro->category}}
          </a>
          @endforeach
        </div>
      </div>
      <div class="col col-md-4 cont2">
        <div class="list-items program-list">
          <div class="btn-program">
            <h1 class="head btn">โปรแกรมใหม่ล่าสุด</h1>
          </div>
          <div class="program-lists list-group program-list-group">
            @foreach ($programsLast as $pro)
            <?php
              $string = $pro->text;
              $replaced = str_replace(['&emsp;','<p>','</p>','<strong>','</strong>','&ensp;','&nbsp;','<b>','<i>','<u>','<h1>','<h2>','<h3>','</b>','</i>','</u>','</h1>','</h2>','</h3>','<br>','</br>'], "", $string);
            ?>
            <a href="/program/{{$pro->slug}}" class="program-content list-group-item list-group-item-action">
              <div class="program-list-content">
                <div class="program-list-image" alt="Line">
                  <img src="{{$pro->icon}}" />
                </div>
                <div class="program-list-text">
                  <span>{{$pro->name}}</span>
                  <p>{{$replaced}}</p>
                  <i class="calendar fa fa-calendar-alt" aria-hidden="false">
                    <sub>{{ date('d-M-Y', strtotime($pro->created_at)) }}</sub>
                  </i>
                  <i class="fa fa-folder pin" aria-hidden="false">
                      <sub>{{$pro->category}}</sub>
                  </i>
                  <button class="btn btn-success hide">></button>
                </div>
              </div>
            </a>
            @endforeach
          </div>
        </div>
        <div class="program-btn-more">
          <a href="/programs">
            <button class="btn-more btn btn-outline-primary">
              เพิ่มเติม
            </button>
          </a>
        </div>
      </div>
      <div class="col col-md-4 cont3">
        <div class="list-items games-list">
          <div class="btn-games">
            <h1 class="head btn">แอพพลิเคชั่นใหม่ล่าสุด</h1>
          </div>
          <div class="list-group games-list-group">
            @foreach ($applicationsLast as $app)
            <?php
              $string = $app->text;
              $replaced = str_replace(['&emsp;','<p>','</p>','<strong>','</strong>','&ensp;','&nbsp;','<b>','<i>','<u>','<h1>','<h2>','<h3>','</b>','</i>','</u>','</h1>','</h2>','</h3>','<br>','</br>'], "", $string);
            ?>
            <a href="/application/{{$app->slug}}" class="games-content list-group-item list-group-item-action">
              <div class="games-list-content">
                <div class="games-list-image" alt="Line">
                  <img src="{{$app->icon}}" />
                </div>
                <div class="games-list-text">
                  <span>{{$app->name}}</span>
                  <p>{{$replaced}}</p>
                  <i class="calendar fa fa-calendar-alt" aria-hidden="false">
                    <sub>{{ date('d-M-Y', strtotime($app->created_at)) }}</sub>
                  </i>
                  <i class="fa fa-folder pin" aria-hidden="false">
                      <sub>{{$app->category}}</sub>
                  </i>
                  <button class="btn btn-success hide">></button>
                </div>
              </div>
            </a>
            @endforeach
          </div>
        </div>
        <div class="games-btn-more">
          <a href="/applications">
            <button class="btn-more btn btn-outline-primary btn-programs">
              เพิ่มเติม
            </button>
          </a>
        </div>
      </div>
      <div class="col-md-2 cont4">
        <h2 class="games-label">หมวดหมู่แอพพลิเคชั่น</h2>
        <div class="list-group games-list">
          @foreach ($categoriesApplications as $catApp)
          <a href="/applications/category/{{$catApp->category}}" class="list-group-item list-group-item-action list-group-item-light">
            {{$catApp->category}}
          </a>
          @endforeach
        </div>
      </div>
    </div>
  </div>
</section>
<section class="section-app">
  <div class="container-lg">
    <div class="row">
      <div class="games-content col-12 animation-area">
        <div class="btn-program label-game">
          <h1>เกมส์ยอดนิยม</h1>
        </div>
        @foreach ($gamesHit as $game)
        <div class="games-gard">
          <a href="/game/{{$game->slug}}">
            <div class="games-image" alt="{{$game->name}}">
              <img src="{{$game->icon}}" />
              <i class="calendar fa fa-calendar-alt" aria-hidden="false">
                {{ date('d-M-Y', strtotime($game->date_out)) }}
              </i>
            </div>
          </a>
        </div>
        @endforeach
        <div class="next-more">
          <a href="/games">
            <i class="far fa-arrow-alt-circle-right"></i>
          </a>
        </div>
        <ul class="box-area">
          <li></li>
          <li></li>
          <li></li>
          <li></li>
          <li></li>
          <li></li>
        </ul>
      </div>
    </div>
  </div>
  
</section>
@endsection