@extends('layouts/template')
@section('title', $game->name)
@section('css')
  <link rel="stylesheet" href="{{URL::asset('css/games.css')}}">
  <link rel="stylesheet" href="{{URL::asset('css/owl.carousel.min.css')}}">
  <link rel="stylesheet" href="{{URL::asset('css/owl.theme.default.min.css')}}">
@endsection
@section('js')
  <script src="{{URL::asset('js/jquery.min.js')}}"></script>
  <script src="{{URL::asset('js/owl.carousel.min.js')}}"></script>
  <script src="{{URL::asset('js/set.owl1.js')}}"></script>
  <script async defer crossorigin="anonymous" src="https://connect.facebook.net/th_TH/sdk.js#xfbml=1&version=v5.0&appId=459791464227500&autoLogAppEvents=1"></script>
@endsection
@section('content')
<section class="section-game-content" style="background-image: url('{{$game->pic}}'); ">
  <div class="contianer-lg">
    <div class="row">
      <div class="col col-sm-6 content desktop">
        <p class="navigation"><a href="/"><i class="fas fa-home"> หน้าแรก</i></a>-> <a href="/games">เกมส์</a>-> {{$game->name}}</p>
        <div class="games-gard">
          <div class="games-image" alt="">
            <img src="{{$game->icon}}" />
            <h2>{{$game->name}}</h2>
          </div>
        </div>
        <div class="info">
          <ul>
            <li>
              <div class="icon">
                <i class="far fa-building"></i>
              </div>
              <div class="text">
                <p>
                  <b>บริษัท: </b>
                  {{$game->company}}
                </p>
              </div>
            </li>
            <li>
              <div class="icon">
                <i class="fas fa-calendar"></i>
              </div>
              <div class="text">
                <p>
                  <b>ออกจำหน่าย:</b> {{ date('d-M-Y', strtotime($game->date_out)) }}
                </p>
              </div>
            </li>
            <li>
              <div class="icon">
                <i class="fas fa-file"></i>
              </div>
              <div class="text">
                <p>
                  <b>ขนาดไฟล์:</b> {{$game->size}}
                </p>
              </div>
            </li>
            <li>
              <div class="icon">
                <i class="fas fa-tag"></i>
              </div>
              <div class="text">
                <p>
                  <a href="/games/category/{{$game->category}}">
                    {{$game->category}}
                  </a>
                </p>
              </div>
            </li>
          </ul>
          <br />
          <ul class>
            <li>
              <div class="text">
                <p>
                  <u>สเปคขั้นต่ำ</u>
                </p>
              </div>
            </li>
            <li>
              <div class="icon">
                <i class="fab fa-windows"></i>
              </div>
              <div class="text">
                <p>
                  <b>OS: </b>
                  {{$game->os}}
                </p>
              </div>
            </li>
            <li>
              <div class="icon">
                <i class="fas fa-microchip"></i>
              </div>
              <div class="text">
                <p>
                  <b>CPU:</b> {{$game->cpu}}
                </p>
              </div>
            </li>
            <li>
              <div class="icon">
                <i class="fas fa-memory"></i>
              </div>
              <div class="text">
                <p>
                  <b>RAM:</b> {{$game->ram}}
                </p>
              </div>
            </li>
            <li>
              <div class="icon">
                <i class="fas fa-ruler"></i>
              </div>
              <div class="text">
                <p>
                  <b>GPU:</b> {{$game->grafig}}
                </p>
              </div>
            </li>
            <li>
              <div class="icon">
                <i class="fas fa-hdd"></i>
              </div>
              <div class="text">
                <p>
                  <b>HDD: </b>
                  {{$game->disk}}
                </p>
              </div>
            </li>
          </ul>
        </div>
      </div>

      <div class="col col-sm-12 content phone">
        <p class="navigation"><a href="/"><i class="fas fa-home"> หน้าแรก</i></a>-> <a href="/games">เกมส์</a>-> {{$game->name}}</p>
        <div class="games-gard">
          <div class="games-image" alt="">
            <img src="{{$game->icon}}" />
            <h2>{{$game->name}}</h2>
          </div>
        </div>
      </div>

      <div class="col col-sm-12 content phone">
        <div class="info">
          <ul>
            <li>
              <div class="icon">
                <i class="far fa-building"></i>
              </div>
              <div class="text">
                <p>
                  <b>บริษัท: </b>
                  {{$game->company}}
                </p>
              </div>
            </li>
            <li>
              <div class="icon">
                <i class="fas fa-calendar"></i>
              </div>
              <div class="text">
                <p>
                  <b>ออกจำหน่าย:</b> {{ date('d-M-Y', strtotime($game->date_out)) }}
                </p>
              </div>
            </li>
            <li>
              <div class="icon">
                <i class="fas fa-file"></i>
              </div>
              <div class="text">
                <p>
                  <b>ขนาดไฟล์:</b> {{$game->size}}
                </p>
              </div>
            </li>
            <li>
              <div class="icon">
                <i class="fas fa-tag"></i>
              </div>
              <div class="text">
                <p>
                  <a href="/games/category/{{$game->category}}">
                    {{$game->category}}
                  </a>
                </p>
              </div>
            </li>
          </ul>
          <br />
          <ul class>
            <li>
              <div class="text">
                <p>
                  <u>สเปคขั้นต่ำ</u>
                </p>
              </div>
            </li>
            <li>
              <div class="icon">
                <i class="fab fa-windows" ></i>
              </div>
              <div class="text">
                <p>
                  <b>OS: </b>
                  {{$game->os}}
                </p>
              </div>
            </li>
            <li>
              <div class="icon">
                <i class="fas fa-microchip"></i>
              </div>
              <div class="text">
                <p>
                  <b>CPU:</b> {{$game->cpu}}
                </p>
              </div>
            </li>
            <li>
              <div class="icon">
                <i class="fas fa-memory" ></i>
              </div>
              <div class="text">
                <p>
                  <b>RAM:</b> {{$game->ram}}
                </p>
              </div>
            </li>
            <li>
              <div class="icon">
                <i class="fas fa-ruler" ></i>
                "&gt;
              </div>
              <div class="text">
                <p>
                  <b>GPU:</b> {{$game->grafig}}
                </p>
              </div>
            </li>
            <li>
              <div class="icon">
                <i class="fas fa-hdd" ></i>
              </div>
              <div class="text">
                <p>
                  <b>HDD: </b>
                  {{$game->disk}}
                </p>
              </div>
            </li>
          </ul>
        </div>
      </div>

    </div>
  </div>
</section>
<section class="section-game-content2">
  <div class="contianer-lg">
    <div class="row">
      <div class="col col-sm-7 contents desktop">
        <div class="content">
          <p>{!!$game->info!!}</p>
        </div>
        <div class="download">
          <h1>DOWNLOAD {{$game->name}}</h1>
          <ul>
            <li>
              @foreach ($gameLinks as $link)
                @if($link->type == 1)
                <div class="one-part">
                  <sub>*โหลดไฟล์เดียว</sub>
                    <?php $replaced = str_replace(['/'], "~", $link->link);?>
                    <a href="/increment/2/{{$game->id}}/{{$replaced}}" target="_blank">
                      <button class="btn btn-sm btn-primary" onClick="">
                        <i class="fas fa-download"></i>
                        DOWNLOAD 1 PART
                      </button>
                    </a>
                  </div>
                  @endif
                @endforeach
            </li>
            <li>
              <div class="part">
                @foreach ($gameLinks as $link)
                  @if($link->type == 1 && $link->type == 0)
                    <sub>*โหลดแบ่งไฟล์</sub>
                  @endif
                  @if($link->type == 0)
                    <?php $replaced1 = str_replace(['/'], "~", $link->link);?>
                    <a href="/increment/2/{{$game->id}}/{{$replaced1}}" target="_blank">
                      <button class="btn btn-sm btn-success" onClick="">
                        <i class="fas fa-long-arrow-alt-down"></i>
                        PART{{$link->part}}
                      </button>
                    </a>
                  @endif
                @endforeach
              </div>
            </li>
            <li>
              <div class="crack">
                @foreach ($gameLinks as $link)
                  @if($link->type == 2)
                    <?php $replaced2 = str_replace(['/'], "~", $link->link);?>
                    <a href="/increment/2/{{$game->id}}/{{$replaced2}}" target="_blank">
                      <button class="btn btn-sm btn-warning" onClick="">
                        <i class="fas fa-cog" ></i>
                        Crack
                      </button>
                    </a>
                  @elseif($link->type == 3)
                    <?php $replaced3 = str_replace(['/'], "~", $link->link);?>
                    <a href="/increment/2/{{$game->id}}/{{$replaced3}}" target="_blank">
                    <button
                      style="margin-left: 2px"
                      class="btn btn-sm btn-danger"
                      onClick=""
                    >
                      <i class="fas fa-file"></i>
                      Patch
                    </button>
                  </a>
                  @endif
                @endforeach
              </div>
            </li>
            <li></li>
          </ul>
        </div>
        <div class="tags">
          <p class="tag">Tags</p>
          <p class="tagArr">
            @if($game->tag != null)
              @foreach(explode(',',$game->tag) as $tag)
                <a href="/tag/{{$tag}}">
                  <i class="far fa-dot-circle fa-xs"></i>
                  {{$tag}}
                </a>
              @endforeach
            @else
              Empty Tags
            @endif
            
          </p>
        </div>
        <div id="fb-root" style="margin-left: 25px">
          <div class="fb-like" data-href="/game/{{$game->category}}/{{$game->id}}" data-width="" data-layout="button_count" data-action="like" data-size="small" data-share="true"></div>
        </div>
        <div style="margin-left: 25px" class="fb-comments" data-href="www.localhost" data-width="100%" data-numposts="3"></div>

        <button
          class="btn btn-dark"
          style="margin-left: 30px; margin-bottom: 10px; margin-top: 10px; color: white;"
        >
          <i class="fas fa-angle-left"></i>
          <a href="/games" style="color: white">
            กลับ
          </a>
        </button>
      </div>
      <div class="col col-sm-5 media desktop">
        <div class="video">
          <iframe
            width="100%"
            height="300px"
            src="https://www.youtube.com/embed/{{$game->video}}?rel=0&autoplay=1&mute=1"
          ></iframe>
        </div>
        <div class="owl-carousel owl-theme carousel">
          <div class="item carousel-crad">
              <div class="gard-image col">
                <img src="{{$game->pic}}" class="img-fluid img-content" alt="{{$game->name}}" />
              </div>
          </div>
          @if($game->pic2 != null)
            <div class="item carousel-crad">
              <div class="gard-image col">
                <img src="{{$game->pic2}}" class="img-fluid img-content" alt="{{$game->name}}" />
              </div>
          </div>
          @endif
          @if($game->pic3 != null)
          <div class="item carousel-crad">
            <div class="gard-image col">
              <img src="{{$game->pic3}}" class="img-fluid img-content" alt="{{$game->name}}" />
            </div>
          </div>
          @endif
          @if($game->pic4 != null)
          <div class="item carousel-crad">
            <div class="gard-image col">
              <img src="{{$game->pic4}}" class="img-fluid img-content" alt="{{$game->name}}" />
            </div>
          </div>
          @endif
        </div>
      </div>

      

      <div class="col col-sm-12 media phone">
        <div class="owl-carousel owl-theme carousel">
          <div class="item carousel-crad">
              <div class="gard-image col">
                <img src="{{$game->pic}}" class="img-fluid img-content" alt="{{$game->name}}" />
              </div>
          </div>
          @if($game->pic2 != null)
            <div class="item carousel-crad">
              <div class="gard-image col">
                <img src="{{$game->pic2}}" class="img-fluid img-content" alt="{{$game->name}}" />
              </div>
          </div>
          @endif
          @if($game->pic3 != null)
          <div class="item carousel-crad">
            <div class="gard-image col">
              <img src="{{$game->pic3}}" class="img-fluid img-content" alt="{{$game->name}}" />
            </div>
          </div>
          @endif
          @if($game->pic4 != null)
          <div class="item carousel-crad">
            <div class="gard-image col">
              <img src="{{$game->pic4}}" class="img-fluid img-content" alt="{{$game->name}}" />
            </div>
          </div>
          @endif
        </div>
      </div>
      <div class="col col-sm-12 contents phone">
        <div class="content">
          <p>{!!$game->info!!}</p>
        </div>
        <div class="download">
          <h1>DOWNLOAD {{$game->name}}</h1>
          <ul>
            <li>
              @foreach ($gameLinks as $link)
                @if($link->type == 1)
                <div class="one-part">
                  <sub>*โหลดไฟล์เดียว</sub>
                    <?php $replaced = str_replace(['/'], "~", $link->link);?>
                    <a href="/increment/2/{{$game->id}}/{{$replaced}}" target="_blank">
                      <button class="btn btn-sm btn-primary" onClick="">
                        <i class="fas fa-download"></i>
                        DOWNLOAD 1 PART
                      </button>
                    </a>
                  </div>
                  @endif
                @endforeach
            </li>
            <li>
              <div class="part">
                @foreach ($gameLinks as $link)
                  @if($link->type == 1)
                    <sub>*โหลดแบ่งไฟล์</sub>
                  @endif
                  @if($link->type == 0)
                    <?php $replaced1 = str_replace(['/'], "~", $link->link);?>
                    <a href="/increment/2/{{$game->id}}/{{$replaced1}}" target="_blank">
                      <button class="btn btn-sm btn-success" onClick="">
                        <i class="fas fa-long-arrow-alt-down"></i>
                        PART{{$link->part}}
                      </button>
                    </a>
                  @endif
                @endforeach
              </div>
            </li>
            <li>
              <div class="crack">
                @foreach ($gameLinks as $link)
                  @if($link->type == 2)
                    <?php $replaced2 = str_replace(['/'], "~", $link->link);?>
                    <a href="/increment/2/{{$game->id}}/{{$replaced2}}" target="_blank">
                      <button class="btn btn-sm btn-warning" onClick="">
                        <i class="fas fa-cog" ></i>
                        Crack
                      </button>
                    </a>
                  @elseif($link->type == 3)
                    <?php $replaced3 = str_replace(['/'], "~", $link->link);?>
                    <a href="/increment/2/{{$game->id}}/{{$replaced3}}" target="_blank">
                    <button
                      style="margin-left: 2px"
                      class="btn btn-sm btn-danger"
                      onClick=""
                    >
                      <i class="fas fa-file"></i>
                      Patch
                    </button>
                  </a>
                  @endif
                @endforeach
              </div>
            </li>
            <li></li>
          </ul>
        </div>

        <div class="tags">
          <p class="tag">Tags</p>
          <p class="tagArr">
            @if($game->tag != null)
              @foreach(explode(',',$game->tag) as $tag)
                <a href="/tag/{{$tag}}">
                  <i class="far fa-dot-circle fa-xs"></i>
                  {{$tag}}
                </a>
              @endforeach
            @else
              Empty Tags
            @endif
          </p>
        </div>

        <div id="fb-root" style="margin-left: 10px">
          <div class="fb-like" data-href="/game/{{$game->category}}/{{$game->id}}" data-width="" data-layout="button_count" data-action="like" data-size="small" data-share="true"></div>
        </div>
        <div class="fb-comments" data-href="www.localhost" data-width="" data-numposts="3"></div>

        
        <button
          class="btn btn-dark"
          style="margin-left: 30px; margin-bottom: 10px; margin-top: 10px; color: white;"
        >
          <i class="fas fa-angle-left"></i>
          <a href="/games" style="color: white">
            กลับ
          </a>
        </button>
      </div>
    </div>
  </div>
</section>
@endsection