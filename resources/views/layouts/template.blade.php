
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title')</title>
    <meta name="description" content="@yield('description')" />
    <meta property="og:url"           content="localhost" />
    <meta property="og:type"          content="website" />
    <meta property="og:title"         content="@yield('title')" />
    <meta property="og:description"   content="@yield('description')" />
    <meta property="og:image"         content="{{ URL::asset('img/icon/p-logo.png') }}" />
    
    <link rel="icon" href="{{ URL::asset('img/icon/p-logo.png') }}" type="image/x-icon"/>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" 
    integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link
      rel="stylesheet"
      href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
      integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf"
      crossorigin="anonymous"
    />
    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="{{URL::asset('css/nav.css')}}">
    <link rel="stylesheet" href="{{URL::asset('css/style-scollbar.css')}}">
    <link rel="stylesheet" href="{{URL::asset('css/footer.css')}}">
    @yield('css')
    
</head>
<body>
    <div>
        <!--Navbar-->
        <nav class="navbar navbar-dark py-0 bg-primary navbar-expand-lg py-md-0">
        <!-- Brand/logo -->
        <a class="navbar-brand" href="/">
            <img
            src="{{URL::asset('/img/icon/pag-load-logo.png')}}"
            alt="logo"
            style="width:40px;"
            />
        </a>

        <button
            class="navbar-toggler"
            type="button"
            data-toggle="collapse"
            data-target="#collapse_Navbar"
        >
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="collapse_Navbar">
            <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" href="/"><button class="btn btn-primary">หน้าแรก</button></a>
            </li>

            <li class="nav-item">
                <a class="nav-link" href="/programs"><button class="btn btn-primary">โปรแกรม</button></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/applications"><button class="btn btn-primary">แอพพลิเคชั่น</button></a>
            </li>

            <li class="nav-item">
                <a class="nav-link" href="/games"><button class="btn btn-primary active">เกมส์</button></a>
            </li>
            </ul>
            <form class="form-inline form-phone" action="/search" method="GET" role="search">
              <input class="form-control mr-sm-2" type="text" name="word" placeholder="ค้นหาสิ่งที่ต้องการ..."/>
              <button class="btn btn-primary" type="submit">
                ค้นหา
              </button>
            </form>
        </div>

        <form class="form-inline form-desktop" action="/search" method="GET">
            <input class="form-control mr-sm-2" type="text" name="word" placeholder="ค้นหาสิ่งที่ต้องการ..."/>
            <button class="btn btn-primary" type="submit">ค้นหา</button>
        </form>
        </nav>
    </div>

    @yield('content')

    <section class="section-footer">
          <!-- Site footer -->
            <footer class="site-footer">
                <div class="container-lg">
                  <div class="row" style="padding-left: 20px; padding-right: 20px">
                    <div class="col-sm-12 col-md-6">
                      <h6>เกี่ยวกับเรา</h6>
                      <p class="text-justify"><strong>PAGLoad เว็บไซต์โหลดโปรแกรมฟรี โหลดแอพฟรี โหลดเกมส์ pc ฟรี</strong> เช่น  idm ถาวร 
                        โปรแกรมตัดต่อวีดีโอ โปรแกรมแต่งรูป โปรแกรมสแกนไวรัส windows10 แอปแต่งรูป แอพตัดต่อวิดีโอ แอพโหลดวีดีโอ แอพโหลดหนัง  เกมผจญภัย เกมต่อสู้ เกมแข่งรถ เกมรถ เกมผี เกมยิ่งปื่น</p>
                    </div>
          
                    <div class="col-xs-6 col-md-3">
                      <h6>หมวดหมู่</h6>
                      <ul class="footer-links">
                        <li><a href="/programs">โปรแกรม</a></li>
                        <li><a href="/applications">แอพพลิเคชั่น</a></li>
                        <li><a href="/games">เกมส์</a></li>
                      </ul>
                    </div>
          
                    <div class="col-xs-6 col-md-3">
                      <h6>ติดต่อเรา</h6>
                      <ul class="footer-links">
                        <li><i class="fab fa-facebook-f"></i></i> facebook.com/DLGCFree</li>
                        <li><i class="fas fa-at"></i> pgmoney1955@gmail.com</li>
                      </ul>
                    </div>
                  </div>
                  <hr>
                </div>
                <div class="container">
                  <div class="row">
                    <div class="col-md-8 col-sm-6 col-xs-12">
                      <p class="copyright-text">Copyright &copy; 2020 Version 1.0 All Rights Reserved by 
                  <a href="https://web.facebook.com/DLGCFree" target="_blank">Pg1955</a>.
                      </p>
                    </div>
          
                    <div class="col-md-4 col-sm-6 col-xs-12">
                      <ul class="social-icons">
                        <li><a class="facebook" href="https://web.facebook.com/DLGCFree" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                        <li><a class="twitter" href="#"><i class="fab fa-twitter" target="_blank"></i></a></li>
                        <li><a class="dribbble" href="#"><i class="fab fa-dribbble"></i></a></li>
                        <li><a class="linkedin" href="#"><i class="fab fa-linkedin"></i></a></li>   
                      </ul>
                    </div>
                  </div>
                </div>
          </footer>
          </section>
</body>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
@yield('js')
</html>
@yield('end-page')