@extends('layouts/template')
@section('title', $tag)
@section('css')
<link rel="stylesheet" href="{{URL::asset('css/programs.css')}}">
@endsection
@section('content')
<section class="section-content">
  <div class="container-lg">
    <div class="row">
      <div class="col-md-2 cont1">
        
      </div>
      <div class="col col-md-6 cont2">
        <h1 class="label-last" style="margin-top: 15px">
          แท็ก: "{{$tag}}"
        </h1>
          <div class="list-items program-list" style="margin-top: 85px">
              <div class="list-group list-group-flush program-list-group ">
                  @foreach ($data as $item)
                  <?php
                  $string = $item->info;
                  $replaced = str_replace(['<strong>','</strong>','<p>','</p>','<b>','<i>','<u>','<h1>','<h2>','<h3>','</b>','</i>','</u>','</h1>','</h2>','</h3>','<br>','</br>'], "", $string);
                  ?>
                  <a 
                  @if($item->application == 0)
                    href="/program/{{$item->slug}}" 
                  @elseif($item->application == 1)
                    href="/application/{{$item->slug}}" 
                  @else
                    href="/game/{{$item->slug}}" 
                  @endif
                    class="program-content list-group-item list-group-item-action"
                  >
                      <div class="program-list-content">
                          <div class="program-list-image"><img src="{{$item->icon}}"/></div>
                          <div class="program-list-text">
                              <span>{{$item->name}}</span>
                              <p>{{$replaced}}</p>
                              <i class="calendar fa fa-calendar-alt" aria-hidden="false">
                                  <sub>{{ date('d-M-Y', strtotime($item->created_at)) }}</sub>
                              </i>
                              <i class="fa fa-folder pin" aria-hidden="false">
                                    <sub>{{$item->category}}</sub>
                                </i>
                              <button class="btn btn-success">Download</button>
                          </div>
                      </div>
                  </a>
                  @endforeach
              </div>
              <div class="pagination">{{$data->links()}}</div>
            </div>
      </div>
      <div class="col md col-1 dummy">
      </div>
      <div class="col md col-3 cont3 phone-hide">
        <div class="list-items list-more">
          <h2 class="label-group">
            โปรแกรมที่น่าสนใจ
          </h2>
          <div class="list-group program-list-more">
            @foreach ($programsHit as $proHit)
                <?php
                  $string = $proHit->text;
                  $replaced = str_replace(['<strong>','</strong>','<p>','</p>','<b>','<i>','<u>','<h1>','<h2>','<h3>','</b>','</i>','</u>','</h1>','</h2>','</h3>','<br>','</br>'], "", $string);
                ?>
            <a href="/program/{{$proHit->slug}}" class="list-more-content list-group-item list-group-item-action" target="_blank">
            <div class="list-content">
              <div class="list-image col">
                <img src="{{$proHit->icon}}" />
              </div>
              <div class="list-text col">
                <span>{{$proHit->name}}</span>
                <p>{{$replaced}}</p>
              </div>
            </div>
          </a>
          @endforeach
          </div>
        </div>
        <div class="list-items list-more">
          <h2 class="label-group">
            แอพพลิเคชั่นที่น่าสนใจ
          </h2>
          <div class="list-group app-list">
            @foreach ($applicationsHit as $appHit)
                <?php
                  $string = $appHit->text;
                  $replaced = str_replace(['<strong>','</strong>','<p>','</p>','<b>','<i>','<u>','<h1>','<h2>','<h3>','</b>','</i>','</u>','</h1>','</h2>','</h3>','<br>','</br>'], "", $string);
                ?>
            <a href="/application/{{$appHit->slug}}" class="list-more-content list-group-item list-group-item-action" target="_blank">
            <div class="list-content">
              <div class="list-image col">
                <img src="{{$appHit->icon}}" />
              </div>
              <div class="list-text col">
                <span>{{$appHit->name}}</span>
                <p>{{$replaced}}</p>
              </div>
            </div>
          </a>
          @endforeach
          </div>
        </div>
        <div class="list-items list-more">
          <h2 class="label-games">เกมส์ที่น่าสนใจ</h2>
          <div class="list-games">
            @foreach ($gamesHit as $game)
            <div class="games-gard">
              <a href="/game/{{$game->slug}}" target="_blank">
                <div class="games-image" alt="">
                  <img src="{{$game->icon}}" />
                  <i class="calendar fa fa-calendar-alt" aria-hidden="false">
                    {{ date('d-M-Y', strtotime($game->date_out)) }}
                  </i>
                </div>
              </a>
            </div>
            @endforeach
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection