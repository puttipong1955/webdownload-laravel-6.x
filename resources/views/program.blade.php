@extends('layouts/template')
@section('title', $program->name)
@section('css')
<link rel="stylesheet" href="{{URL::asset('css/programs.css')}}">
<link rel="stylesheet" href="{{URL::asset('css/owl.carousel.min.css')}}">
<link rel="stylesheet" href="{{URL::asset('css/owl.theme.default.min.css')}}">
@endsection
@section('js')
<script src="{{URL::asset('js/jquery.min.js')}}"></script>
<script src="{{URL::asset('js/owl.carousel.min.js')}}"></script>
<script src="{{URL::asset('js/set.owl1.js')}}"></script>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/th_TH/sdk.js#xfbml=1&version=v5.0&appId=459791464227500&autoLogAppEvents=1"></script>
@endsection
@section('content')
<div id="fb-root"></div>
<section class="section-content">
  <div class="container-lg">
    <div class="row">
      <div class="col-md-12 navigation"><p class="navigation"><a href="/"><i class="fas fa-home"> หน้าแรก</i></a>> <a href="/programs">โปรแกรม</a>> {{$program->name}}</p></div>
      <div class="col-md-2 cont1">
        <span class="label-cet">หมวดหมู่</span>
        <div class="list-group cetagories-list">
          @foreach ($categories as $cat)
          <a href="/programs/category/{{$cat->category}}"}
          class="cate-list-item list-group-item list-group-item-action list-group-item-light">
            {{$cat->category}}
          </a>
          @endforeach
        </div>
      </div>

      <div class="col md col-7 program-detail programDesktop">
        <img src='{{$program->icon}}' class="imgIcon" />
        <h1>{{$program->name}}</h1>
        <p>{!! $program->text !!}</p>
        <div class="owl-carousel owl-theme carousel col col-md-12 col-12 carousel-phone">
          <div class="item carousel-crad">
              <div class="gard-image col">
                <img src="{{$program->pic}}" class="img-fluid" alt="{{$program->name}}" />
              </div>
          </div>
          @if($program->pic2 != null)
            <div class="item carousel-crad">
              <div class="gard-image col">
                <img src="{{$program->pic2}}" class="img-fluid" alt="{{$program->name}}" />
              </div>
          </div>
          @endif
          @if($program->pic3 != null)
          <div class="item carousel-crad">
            <div class="gard-image col">
              <img src="{{$program->pic3}}" class="img-fluid" alt="{{$program->name}}" />
            </div>
        </div>
          @endif
        </div>
        <div class="owl-carousel owl-theme carousel col col-md-8 col-8 carousel-desktop">
            <div class="item carousel-crad">
                <div class="gard-image col">
                  <img src="{{$program->pic}}" class="img-fluid img-content" alt="{{$program->name}}" />
                </div>
            </div>
            @if($program->pic2 != null)
              <div class="item carousel-crad">
                <div class="gard-image col">
                  <img src="{{$program->pic2}}" class="img-fluid img-content" alt="{{$program->name}}" />
                </div>
            </div>
            @endif
            @if($program->pic3 != null)
            <div class="item carousel-crad">
              <div class="gard-image col">
                <img src="{{$program->pic3}}" class="img-fluid img-content" alt="{{$program->name}}" />
              </div>
          </div>
          @endif
        </div>
        @if($program->pic == null)
        <div class="col col-md-4 col-4 download" style="display: none;">
        @else
        <div class="col col-md-4 col-4 download">
        @endif
          <?php $replaced = str_replace(['/'], "~", $program->link);?>
          <a href="/increment/0/{{$program->id}}/{{$replaced}}" target="_blank">
          <button class="btn btn-success" name="download">
              DOWNLOAD
              <i class="fas fa-file-download"></i>
            </button>
          </a>
          <div class="download-info">
            <ul class="list-group list-group-flush">
              <li class="list-group-item">
                ขนาด: {{$program->size}}
              </li>
              <li class="list-group-item">
                หมวดหมู่: <a href="/programs/category/{{$program->category}}" style="color: dimgray">{{$program->category}}</a>
              </li>
              <li class="list-group-item">
              ระบบปฏิบัติการ: {{$program->os}}
              </li>
              <li class="list-group-item">
                เวอร์ชัน: {{$program->version}}
              </li>
              <li class="list-group-item">
                สิทธิ์การใช้งาน: {{$program->copyright}}
              </li>
            </ul>
          </div>
        </div>
        <div class="what-new">
          @if($program->news!=null)
            <h2 class="new">คุณสมบัติเวอร์ชั่นนี้</h2>
          @endif
          <p>{!! $program->news !!}</p>
        </div>
        <div class="tags">
          <h3 class="tag">Tags</h3>
          <p>
            @if($program->tag != null)
              @foreach(explode(',',$program->tag) as $tag)
                <a href="/tag/{{$tag}}">
                  <i class="far fa-dot-circle fa-xs"></i>
                  {{$tag}}
                </a>
              @endforeach
            @else
              Empty Tags
            @endif
          </p>
        </div>
        <div id="fb-root">
          <div class="fb-like" data-href="/program/{{$program->slug}}" data-width="" data-layout="button_count" data-action="like" data-size="small" data-share="true"></div>
        </div>
        <p class="prosame">โปรแกรมที่คล้ายกัน</p>
        <div class="release-program">
          @foreach ($programsRandomCategory as $proRandom)
          <div class="item program-crad">
            <div class="gard-image col">
              <a href="/program/{{$proRandom->slug}}">
                <img src={{$proRandom->icon}} class="img-fluid" />
                <span>{{$proRandom->name}}</span>
              </a>
            </div>
            <div class="gard-content col">
              <p>{!! $program->text !!}</p>
            </div>
          </div>
          @endforeach
        </div>
        <div class="fb-comments" data-href="www.localhost" data-width="" data-numposts="3"></div>
      </div>

      <div class="col md col-12 program-detail programPhone">
        <img src='{{$program->icon}}' class="imgIcon" />
        <h1>{{$program->name}}</h1>
        <p>{!! $program->text !!}</p>
        <div class="owl-carousel owl-theme carousel col col-md-12 col-12 carousel-phone">
          <div class="item carousel-crad">
              <div class="gard-image col">
                <img src="{{$program->pic}}" class="img-fluid" alt="{{$program->name}}" />
              </div>
          </div>
          @if($program->pic2 != null)
            <div class="item carousel-crad">
              <div class="gard-image col">
                <img src="{{$program->pic2}}" class="img-fluid" alt="{{$program->name}}" />
              </div>
          </div>
          @endif
          @if($program->pic3 != null)
          <div class="item carousel-crad">
            <div class="gard-image col">
              <img src="{{$program->pic3}}" class="img-fluid" alt="{{$program->name}}" />
            </div>
        </div>
          @endif
        </div>
        <div class="owl-carousel owl-theme carousel col col-md-8 col-8 carousel-desktop">
            <div class="item carousel-crad">
                <div class="gard-image col">
                  <img src="{{$program->pic}}" class="img-fluid img-content" alt="{{$program->name}}" />
                </div>
            </div>
            @if($program->pic2 != null)
              <div class="item carousel-crad">
                <div class="gard-image col">
                  <img src="{{$program->pic2}}" class="img-fluid img-content" alt="{{$program->name}}" />
                </div>
            </div>
            @endif
            @if($program->pic3 != null)
            <div class="item carousel-crad">
              <div class="gard-image col">
                <img src="{{$program->pic3}}" class="img-fluid img-content" alt="{{$program->name}}" />
              </div>
          </div>
          @endif
        </div>
        <div class="col col-md-4 col-4 download">
          <?php $replaced = str_replace(['/'], "~", $program->link);?>
          <a href="/increment/0/{{$program->id}}/{{$replaced}}" target="_blank">
          <button class="btn btn-success" name="download">
              DOWNLOAD
              <i class="fas fa-file-download"></i>
            </button>
          </a>
          <div class="download-info">
            <ul class="list-group list-group-flush">
              <li class="list-group-item">
                ขนาด: {{$program->size}}
              </li>
              <li class="list-group-item">
                หมวดหมู่: <a href="/programs/category/{{$program->category}}" style="color: dimgray">{{$program->category}}</a>
              </li>
              <li class="list-group-item">
                ระบบปฏิบัติการ: {{$program->os}}
              </li>
              <li class="list-group-item">
                เวอร์ชัน: {{$program->version}}
              </li>
              <li class="list-group-item">
                สิทธิ์การใช้งาน: {{$program->copyright}}
              </li>
            </ul>
          </div>
        </div>
        <div class="what-new">
          @if($program->news!=null)
            <h2 class="new">คุณสมบัติเวอร์ชั่นนี้</h2>
          @endif
          <p>{!! $program->news !!}</p>
        </div>
        <div class="tags">
          <h3 class="tag">Tags</h3>
          <p>
            @if($program->tag != null)
              @foreach(explode(',',$program->tag) as $tag)
                <a href="/tag/{{$tag}}">
                  <i class="far fa-dot-circle fa-xs"></i>
                  {{$tag}}
                </a>
              @endforeach
            @else
              Empty Tags
            @endif
          </p>
        </div>
        
        <div id="fb-root">
          <div class="fb-like" data-href="/program/{{$program->slug}}" data-width="" data-layout="button_count" data-action="like" data-size="small" data-share="true"></div>
        </div>

        <p class="prosame">โปรแกรมที่คล้ายกัน</p>
        <div class="release-program">
          @foreach ($programsRandomCategory as $proRandom)
          <div class="item program-crad">
            <div class="gard-image col">
              <a href="/program/{{$proRandom->slug}}">
                <img src={{$proRandom->icon}} class="img-fluid" />
                <span>{{$proRandom->name}}</span>
              </a>
            </div>
            <div class="gard-content col">
              <p>{!! $program->text !!}</p>
            </div>
          </div>
          @endforeach
        </div>
        <div class="fb-comments" data-href="www.localhost" data-width="100%" data-numposts="3"></div>
      </div>
      
      <div class="col md col-3 more">
        <div class="list-items list-more">
          <h2 class="label-group">
            โปรแกรมที่น่าสนใจ
          </h2>
          <div class="list-group program-list-more">
            @foreach ($programsHit as $proHit)
                <?php
                  $string = $proHit->text;
                  $replaced = str_replace(['<strong>','</strong>','<p>','</p>','<b>','<i>','<u>','<h1>','<h2>','<h3>','</b>','</i>','</u>','</h1>','</h2>','</h3>','<br>','</br>'], "", $string);
                ?>
            <a href="/program/{{$proHit->slug}}" class="list-more-content list-group-item list-group-item-action" target="_blank">
            <div class="list-content">
              <div class="list-image col">
                <img src="{{$proHit->icon}}" />
              </div>
              <div class="list-text col">
                <span>{{$proHit->name}}</span>
                <p>{{$replaced}}</p>
              </div>
            </div>
          </a>
          @endforeach
          </div>
        </div>
        <div class="list-items list-more">
          <h2 class="label-group">
            แอพพลิเคชั่นที่น่าสนใจ
          </h2>
          <div class="list-group app-list">
            @foreach ($applicationsHit as $appHit)
                <?php
                  $string = $appHit->text;
                  $replaced = str_replace(['<b>','<i>','<u>','<h1>','<h2>','<h3>','</b>','</i>','</u>','</h1>','</h2>','</h3>','<br>','</br>'], "", $string);
                ?>
            <a href="/application/{{$appHit->slug}}" class="list-more-content list-group-item list-group-item-action" target="_blank">
            <div class="list-content">
              <div class="list-image col">
                <img src="{{$appHit->icon}}" />
              </div>
              <div class="list-text col">
                <span>{{$appHit->name}}</span>
                <p>{{$replaced}}</p>
              </div>
            </div>
          </a>
          @endforeach
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection