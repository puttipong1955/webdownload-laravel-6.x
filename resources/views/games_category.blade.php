@extends('layouts/template')
@section('title', 'Games All')
@section('css')
<link rel="stylesheet" href="{{URL::asset('css/games.css')}}">
@endsection
@section('js')
    <script src="http://code.jquery.com/jquery-3.3.1.min.js"
    integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
    crossorigin="anonymous">
    </script>
@endsection
@section('end-page')
<script>
  $(document).on('click','.open-category',function(event){
    $("#categoryList").empty();
    var url = "/games/categories";
    $.get(url, function (programsCategory) {
        //success data
        console.log(programsCategory);
        jQuery.each(programsCategory, function(index, value){
          $("#categoryList").append("<a href='/games/category/"+ value['category']+ "'><li class='list-group-item modal-link'>"+ value['category'] + "</li></a>");
          }); 
    }) 
  });  
</script>
@endsection
@section('content')
<section class="section-most animation-area" style="margin-top: -10px">

  <div id="modalCategory" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div style="padding: 10px">
          <div class="form-group">
            <label for="inputAddress2">Links</label>
            <ul id="categoryList" class="list-group list-group-flush" style="align-items: center">
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="container-lg">
    <div class="row">
      <a href="#" class="open-category" style="color: black" data-toggle="modal" data-target=".bd-example-modal-lg">
        <div class="card create"><i class="far fa-folder"></i></div>
      </a>
      <h1 class="game-label">หมวดหมู่: " {{$category}} "</h1>
      <div class="games-content col-12">
        <div>
          @foreach ($gamesCategory as $game)
          <div class="games-gard">
            <a href="/game/{{$game->id}}">
              <div class="games-image" alt="">
                <img src="{{$game->icon}}" />
                <i class="calendar fa fa-calendar-alt" aria-hidden="false">
                  {{ date('d-M-Y', strtotime($game->date_out)) }}
                </i>
                <p>{{$game->name}}</p>
              </div>
            </a>
          </div>
          @endforeach
        </div>
        <ul class="box-area">
          <li></li>
          <li></li>
          <li></li>
          <li></li>
          <li></li>
          <li></li>
        </ul>
      </div>
      <button class="btn btn-dark" style="margin-left: 60px; margin-bottom: 10px; color: white;">
        <i class="fas fa-angle-left"></i>
        <a href="/games" style="color: white">
          กลับ
        </a>
      </button>
    </div>
  </div>
</section>
@endsection