@extends('layouts/template')
@section('title', 'โหลดแอพฟรี โหลดแอพ android ฟรี โหลดแอพ ios ฟรี โหลดแอพเสียตังฟรี')
@section('description', 'โหลดแอปแต่งรูป แอพตัดต่อวิดีโอ แอพโหลดวีดีโอ แอพโหลดหนัง โหลดเกมส์ apk แอพล้างขยะ แอพหาเงิน แอพออกกําลังกาย แอพแชท แอพนำทาง แอพแผนที่ เกมมือถือ garena garena free fire เกมออนไลน์มือถือ')
@section('css')
<link rel="stylesheet" href="css/programs.css">
<link rel="stylesheet" href="{{URL::asset('css/owl.carousel.min.css')}}">
<link rel="stylesheet" href="{{URL::asset('css/owl.theme.default.min.css')}}">
@endsection
@section('js')
<script src="http://code.jquery.com/jquery-3.3.1.min.js"
integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
crossorigin="anonymous">
</script>
<script src="{{URL::asset('js/owl.carousel.min.js')}}">p</script>
<script src="{{URL::asset('js/set.owl.js')}}"></script>
@endsection
@section('end-page')
<script>
  $(document).on('click','.open-category',function(event){
    $("#categoryList").empty();
    var url = "/applications/categories";
    $.get(url, function (applicationsCategory) {
        //success data
        console.log(applicationsCategory);
        jQuery.each(applicationsCategory, function(index, value){
          $("#categoryList").append("<a href='applications/category/"+ value['category']+ "'><li class='list-group-item modal-link'>"+ value['category'] + "</li></a>");
          }); 
    }) 
  });  
</script>
@endsection
@section('content')
    <section class="section-content">

      <div id="modalCategory" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div style="padding: 10px">
              <div class="form-group">
                <label for="inputAddress2">ประเภทแอพพลิเคชั่น</label>
                <ul id="categoryList" class="list-group list-group-flush" style="align-items: center">
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>

        <div class="container-lg">
          <div class="row">
            <a href="#" class="open-category" style="color: black" data-toggle="modal" data-target=".bd-example-modal-lg">
              <div class="card create"><i class="far fa-folder"></i></div>
            </a>
            <div class="col-md-2 cont1">
              <span class="label-cet">หมวดหมู่</span>
              <div class="list-group cetagories-list">
                @foreach ($categories as $cat)
                <a href="/applications/category/{{$cat->category}}"}
                class="cate-list-item list-group-item list-group-item-action list-group-item-light">
                  {{$cat->category}}
                </a>
                @endforeach
              </div>
            </div>
            <div class="col col-md-11 cont2 programPhone">
              <h1 class="label-last">แอพพลิเคชั่นล่าสุด</h1>
                <div class="list-items program-list">
                    <div class="list-group list-group-flush program-list-group ">
                        @foreach ($data as $item)
                        <?php
                        $string = $item->text;
                        $replaced = str_replace(['&emsp;','<p>','</p>','<strong>','</strong>','&ensp;','&nbsp;','<b>','<i>','<u>','<h1>','<h2>','<h3>','</b>','</i>','</u>','</h1>','</h2>','</h3>','<br>','</br>'], "", $string);
                        ?>
                        <a href="/application/{{$item->slug}}" class="program-content list-group-item list-group-item-action">
                            <div class="program-list-content">
                                <div class="program-list-image"><img src="{{$item->icon}}"/></div>
                                <div class="program-list-text">
                                    <span>{{$item->name}}</span>
                                    <p>{{$replaced}}</p>
                                    <i class="calendar fa fa-calendar-alt" aria-hidden="false">
                                        <sub>{{ date('d-M-Y', strtotime($item->created_at)) }}</sub>
                                    </i>
                                    <i class="fa fa-folder pin" aria-hidden="false">
                                          <sub>{{$item->category}}</sub>
                                      </i>
                                    <button class="btn btn-success">Download</button>
                                </div>
                            </div>
                        </a>
                        @endforeach
                    </div>
                    <div class="pagination">{{$data->links()}}</div>
                  </div>
            </div>
            <div class="col col-md-6 cont2 programDesktop">
              <h1 class="label-last">แอพพลิเคชั่นล่าสุด</h1>
                <div class="list-items program-list">
                    <div class="list-group list-group-flush program-list-group ">
                        @foreach ($data as $item)
                        <?php
                        $string = $item->text;
                        $replaced = str_replace(['&emsp;','<p>','</p>','<strong>','</strong>','&ensp;','&nbsp;','<b>','<i>','<u>','<h1>','<h2>','<h3>','</b>','</i>','</u>','</h1>','</h2>','</h3>','<br>','</br>'], "", $string);
                        ?>
                        <a href="/application/{{$item->slug}}" class="program-content list-group-item list-group-item-action">
                            <div class="program-list-content">
                                <div class="program-list-image"><img src="{{$item->icon}}"/></div>
                                <div class="program-list-text">
                                    <span>{{$item->name}}</span>
                                    <p>{{$replaced}}</p>
                                    <i class="calendar fa fa-calendar-alt" aria-hidden="false">
                                        <sub>{{ date('d-M-Y', strtotime($item->created_at)) }}</sub>
                                    </i>
                                    <i class="fa fa-folder pin" aria-hidden="false">
                                          <sub>{{$item->category}}</sub>
                                      </i>
                                    <button class="btn btn-success">Download</button>
                                </div>
                            </div>
                        </a>
                        @endforeach
                    </div>
                    <div class="pagination">{{$data->links()}}</div>
                  </div>
            </div>
            <div class="col md col-1 dummy"></div>
            <div class="col md col-3 cont3 phone-hide">
              <div class="list-items list-more">
                <h2 class="label-group">
                  แอพพลิเคชั่นที่น่าสนใจ
                </h2>
                <div class="list-group app-list">
                  @foreach ($applicationsHit as $appHit)
                      <?php
                        $string = $appHit->text;
                        $replaced = str_replace(['&emsp;','<p>','</p>','<strong>','</strong>','&ensp;','&nbsp;','<b>','<i>','<u>','<h1>','<h2>','<h3>','</b>','</i>','</u>','</h1>','</h2>','</h3>','<br>','</br>'], "", $string);
                      ?>
                  <a href="/application/{{$appHit->slug}}" class="list-more-content list-group-item list-group-item-action" target="_blank">
                  <div class="list-content">
                    <div class="list-image col">
                      <img src="{{$appHit->icon}}" />
                    </div>
                    <div class="list-text col">
                      <span>{{$appHit->name}}</span>
                      <p>{{$replaced}}</p>
                    </div>
                  </div>
                </a>
                @endforeach
                </div>
              </div>
              <div class="list-items list-more">
                <h2 class="label-group">
                  โปรแกรมที่น่าสนใจ
                </h2>
                <div class="list-group program-list-more">
                  @foreach ($programsHit as $proHit)
                      <?php
                        $string = $proHit->text;
                        $replaced = str_replace(['&emsp;','<p>','</p>','<strong>','</strong>','&ensp;','&nbsp;','<b>','<i>','<u>','<h1>','<h2>','<h3>','</b>','</i>','</u>','</h1>','</h2>','</h3>','<br>','</br>'], "", $string);
                      ?>
                  <a href="/program/{{$proHit->slug}}" class="list-more-content list-group-item list-group-item-action" target="_blank">
                  <div class="list-content">
                    <div class="list-image col">
                      <img src="{{$proHit->icon}}" />
                    </div>
                    <div class="list-text col">
                      <span>{{$proHit->name}}</span>
                      <p>{{$replaced}}</p>
                    </div>
                  </div>
                </a>
                @endforeach
                </div>
              </div>
              <div class="list-items list-more">
                <h2 class="label-games">เกมส์ที่น่าสนใจ</h2>
                <div class="list-games">
                  @foreach ($gamesHit as $game)
                  <div class="games-gard">
                    <a href="/game/{{$game->slug}}" target="_blank">
                      <div class="games-image" alt="">
                        <img src="{{$game->icon}}" />
                        <i class="calendar fa fa-calendar-alt" aria-hidden="false">
                          {{ date('d-M-Y', strtotime($game->date_out)) }}
                        </i>
                      </div>
                    </a>
                  </div>
                  @endforeach
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section class="section-slid">
        <div class="container-lg">
            <div class="row">
              <div class="slid col-12">
                <div class="owl-carousel owl-theme carousel">
                @foreach ($applicationsRandom as $item)
                 <div class="item carousel-crad">
                  <div class="gard-image col">
                    <div class="gard-image col">
                      <a href="/application/{{$item->slug}}">
                        <img src="{{$item->icon}}" class="img-fluid" alt="{{$item->name}}" />
                        <span>{{$item->name}}</span>
                      </a>
                    </div>
                    <div class="gard-content col">
                      <?php
                        $replaced = str_replace(['&emsp;','<p>','</p>','<strong>','</strong>','&ensp;','&nbsp;','<b>','<i>','<u>','<h1>','<h2>','<h3>','</b>','</i>','</u>','</h1>','</h2>','</h3>','<br>','</br>'], "", $item->text);
                      ?>
                      <p>{{$replaced}}</p>
                    </div>
                  </div>
                </div>   
                @endforeach
              </div>
              </div>
            </div>
          </div>
        </div>
      </section>
@endsection