@extends('layouts/template')
@section('title', 'โหลดเกมส์ pc ฟรี')
@section('description', 'โหลดเกมส์ฟรี เกม pc เกมไฟล์เดียว เกม pc สเปคต่ํา เกมผจญภัย เกมต่อสู้ เกมแข่งรถ เกมรถ เกมผี เกมยิ่งปื่น เกมกีฬา เกมจําลองสถานการณ์ เกมวางแผน เกมเด็ก')
@section('css')
<link rel="stylesheet" href="css/games.css">
@endsection
@section('js')
    <script src="http://code.jquery.com/jquery-3.3.1.min.js"
    integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
    crossorigin="anonymous">
    </script>
@endsection
@section('end-page')
<script>
  $(document).on('click','.open-category',function(event){
    $("#categoryList").empty();
    var url = "/games/categories";
    $.get(url, function (programsCategory) {
        //success data
        console.log(programsCategory);
        jQuery.each(programsCategory, function(index, value){
          $("#categoryList").append("<a href='games/category/"+ value['category']+ "'><li class='list-group-item modal-link'>"+ value['category'] + "</li></a>");
          }); 
    }) 
  });  
</script>
@endsection
@section('content')
<section class="section-games-hit">

  <div id="modalCategory" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div style="padding: 10px">
          <div class="form-group">
            <label for="inputAddress2">ประเภทเกมส์</label>
            <ul id="categoryList" class="list-group list-group-flush" style="align-items: center">
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="container-lg">
    <div class="row">
      <a href="#" class="open-category" style="color: black" data-toggle="modal" data-target=".bd-example-modal-lg">
        <div class="card create"><i class="far fa-folder"></i></div>
      </a>
      <h1 class="hit-label">เกมส์ยอดนิยม</h1>
      <div class="games-hit-content col-8">
        @foreach ($gamesHit as $game)
        <div class="games-gard">
          <a href="/game/{{$game->slug}}">
            <div class="games-image" alt="">
              <img src="{{$game->icon}}" />
              <i class="calendar fa fa-calendar-alt" aria-hidden="false">
                {{ date('d-M-Y', strtotime($game->date_out)) }}
              </i>
            </div>
          </a>
        </div>
        @endforeach
      </div>
      <div class="side-bar col-4">
      <iframe width="100%" height="315" src="https://www.youtube.com/embed/{{$game1->video}}?rel=0&autoplay=1&mute=1"></iframe>
      <div class="games-gard">
        <a href="/game/{{$game1->slug}}">
          <div class="games-image" alt="">
            <img src={{$game1->icon}} />
            <i class="calendar fa fa-calendar-alt" aria-hidden="false">
              {{ date('d-M-Y', strtotime($game1->date_out))}}
            </i>
            <p>{{$game1->name}}</p>
          </div>
        </a>
      </div>
      <div class="list-items list-more ">
        <div class="list-group list-group-game ">
          <a
            href="/games/category/เกมส์ผจญภัย"
            class="list-cate-content list-group-item list-group-item-action"
          >
            <div class="list-content">
              <div class="list-image col">
                <img src="/img/icon/avengers.png" />
              </div>
              <div class="list-text col">
                <span>เกมส์ผจญภัย</span>
              </div>
            </div>
          </a>
          <a
            href="/games/category/เกมส์ต่อสู้"
            class="list-cate-content list-group-item list-group-item-action"
          >
            <div class="list-content">
              <div class="list-image col">
                <img src="/img/icon/sword.png" />
              </div>
              <div class="list-text col">
                <span>เกมส์ต่อสู้</span>
              </div>
            </div>
          </a>
          <a
            href="/games/category/เกมส์จำลองสถานการณ์จริง"
            class="list-cate-content list-group-item list-group-item-action"
          >
            <div class="list-content">
              <div class="list-image col">
                <img src="/img/icon/simulation.png" />
              </div>
              <div class="list-text col">
                <span>เกมส์จำลองสถานการณ์จริง</span>
              </div>
            </div>
          </a>
          <a
            href="/games/category/เกมส์กีฬา"
            class="list-cate-content list-group-item list-group-item-action"
          >
            <div class="list-content">
              <div class="list-image col">
                <img src="/img/icon/football.png" />
              </div>
              <div class="list-text col">
                <span>เกมส์กีฬา แข่งรถ</span>
              </div>
            </div>
          </a>
          <a
            href="/games/category/เกมส์ผี"
            class="list-cate-content list-group-item list-group-item-action"
          >
            <div class="list-content">
              <div class="list-image col">
                <img src="/img/icon/ghost.png" />
              </div>
              <div class="list-text col">
                <span>เกมส์ผี หลอน</span>
              </div>
            </div>
          </a>
          <a
            href="/games/category/เกมส์เบาสมอง"
            class="list-cate-content list-group-item list-group-item-action"
          >
            <div class="list-content">
              <div class="list-image col">
                <img src="/img/icon/puzzle.png" />
              </div>
              <div class="list-text col">
                <span>เกมส์เบาสมอง</span>
              </div>
            </div>
          </a>
          <a
            href="/games/category/เกมส์วางแผน"
            class="list-cate-content list-group-item list-group-item-action"
          >
            <div class="list-content">
              <div class="list-image col">
                <img src="/img/icon/project-management.png" />
              </div>
              <div class="list-text col">
                <span>เกมส์วางแผน</span>
              </div>
            </div>
          </a>
          <a
            href="/games/category/เกมส์ยิง"
            class="list-cate-content list-group-item list-group-item-action"
          >
            <div class="list-content">
              <div class="list-image col">
                <img src="/img/icon/target.png" />
              </div>
              <div class="list-text col">
                <span>เกมส์ยิง สงคราม</span>
              </div>
            </div>
          </a>
        </div>
      </div>
      </div>
    </div>
  </div>
</section>
<section class="section-most animation-area">
  <div class="container-lg">
    <div class="row">
      <h1 class="game-label">เกมส์ใหม่ ล่าสุด</h1>
      <div class="games-content col-12">
        <div>
          @foreach ($gamesLast as $game)
          <div class="games-gard">
            <a href="/game/{{$game->slug}}">
              <div class="games-image" alt="">
                <img src="{{$game->icon}}" />
                <i class="calendar fa fa-calendar-alt" aria-hidden="false">
                  {{ date('d-M-Y', strtotime($game->date_out))}}
                </i>
                <p>{{$game->name}}</p>
              </div>
            </a>
          </div>
          @endforeach
          <div class="pagination">{{$gamesLast->links()}}</div>
          <ul class="box-area">
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection