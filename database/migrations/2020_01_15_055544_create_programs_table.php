<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProgramsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('programs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->text('text');
            $table->string('category');
            $table->text('tag');
            $table->string('size', 10);
            $table->string('os', 15);
            $table->string('copyright', 10);
            $table->string('version', 50);
            $table->text('news');
            $table->string('link');
            $table->decimal('download', 7, 0);
            $table->decimal('application', 1, 0);
            $table->string('icon');
            $table->string('pic');
            $table->string('pic2');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('programs');
    }
}
