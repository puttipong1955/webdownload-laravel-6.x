<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('games', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->text('info');
            $table->string('company');
            $table->date('date_out');
            $table->string('size', 20);
            $table->string('category', 30);
            $table->text('tag');
            $table->string('cpu', 20);
            $table->string('os', 20);
            $table->string('ram', 20);
            $table->string('grafig', 100);
            $table->string('disk', 30);
            $table->string('video');
            $table->string('icon');
            $table->string('pic');
            $table->string('pic2');
            $table->string('pic3');
            $table->string('pic4');
            $table->string('view');
            $table->decimal('application', 1, 0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('games');
    }
}
