<?php

// Guest
Route::get('/','LandingController@landing');
Route::get('/programs', 'ProgramsController@index');
Route::get('/applications', 'ApplicationsController@index');
Route::get('/games', 'GamesController@index');
Route::get('/programs/category/{category}', 'ProgramsController@category');
Route::get('/applications/category/{category}', 'ApplicationsController@category');
Route::get('/games/category/{category}', 'GamesController@category');
Route::get('/program/{slug}', 'ProgramsController@programSlug');
Route::get('/application/{slug}', 'ApplicationsController@applicationSlug');
Route::get('/increment/{application}/{id}/{link}', 'ProgramsController@increment')->name('increment');
Route::get('/game/{slug}', 'GamesController@gameSlug');
Route::get('/search', 'ProgramsController@search');
Route::get('/tag/{tag}', 'ProgramsController@tag');
Route::get('/programs/categories', 'ProgramsController@getProgramsCategoies')->name('programs-categories');
Route::get('/applications/categories', 'ApplicationsController@getApplicationsCategoies')->name('applications-categories');
Route::get('/games/categories', 'GamesController@getGamesCategoies')->name('games-categories');

Auth::routes();
// Admin
Route::get('/admin', 'AdminController@index')->name('admin');
Route::get('/admin/programs', 'AdminController@programs')->name('admin-programs');
Route::get('/admin/applications', 'AdminController@applications')->name('admin-applications');
Route::get('/admin/games', 'AdminController@games')->name('admin-games');
Route::get('/admin/programs/category/{category}', 'AdminController@programsCategory')->name('admin-programs-category');
Route::get('/admin/applications/category/{category}', 'AdminController@applicationsCategory')->name('admin-applications-category');
Route::get('/admin/games/category/{category}', 'AdminController@gamesCategory')->name('admin-games-category');
Route::get('/admin/search', 'AdminController@search')->name('admin-search');
//Get for show
Route::get('/admin/program/get/{idd}', 'AdminController@search2')->name('admin-program');
Route::get('/admin/game/get/{idgame}', 'AdminController@getGame')->name('admin-game');
Route::get('/admin/game-links/get/{idgame}', 'AdminController@getGameLinks')->name('admin-game-links');
Route::get('/admin/game-link/get/{id}', 'AdminController@getGameLink')->name('admin-game-link');
Route::get('/admin/programs/categories', 'AdminController@getProgramsCategoies')->name('admin-programs-categories');
Route::get('/admin/applications/categories', 'AdminController@getApplicationsCategoies')->name('admin-applications-categories');
Route::get('/admin/games/categories', 'AdminController@getGamesCategoies')->name('admin-games-categories');
//Post
Route::post('/admin/submit-program', 'AdminController@programStore');
Route::post('/admin/submit-application', 'AdminController@applicationStore');
Route::post('/admin/submit-game', 'AdminController@gameStore');
Route::post('/admin/submit-game-link', 'AdminController@gameLinkStore');
//Update
Route::post('/admin/update-program', 'AdminController@programPut');
Route::post('/admin/update-application', 'AdminController@applicationPut');
Route::post('/admin/update-game', 'AdminController@gamePut');
Route::post('/admin/update-game-link', 'AdminController@gameLinkPut');
//Delete
Route::get('/admin/{application}/{id}', 'AdminController@programDel')->name('admin-program-delete');
Route::get('/admin/game-link/del/{id}', 'AdminController@gameLinkDel')->name('admin-game-link-delete');
//Test
Route::get('/admin/test', 'AdminController@test')->name('admin-test');
