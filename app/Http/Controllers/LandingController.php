<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Program;
use App\Game;

class LandingController extends Controller
{
    public function landing(){
        $programsHit = Program::select('id', 'name', 'slug', 'text', 'icon', 'category')
        ->where('application', '0')
        ->where('copyright', 'free')
        ->orderByRaw('download DESC')
        ->limit(5)
        ->get();

        $programsFullHit = Program::select('id', 'name', 'slug', 'text', 'icon', 'category')
        ->where('application', '0')
        ->where('copyright', 'full')
        ->orderByRaw('download DESC')
        ->limit(5)
        ->get();

        $applicationsHit = Program::select('id', 'name', 'slug', 'text', 'icon', 'category')
        ->where('application', '1')
        ->orderByRaw('download DESC')
        ->limit(5)
        ->get();

        $gamesHit5 = Game::select('id', 'name', 'slug', 'info', 'icon')
        ->orderByRaw('view DESC')
        ->limit(5)
        ->get();

        $categoriesPrograms = Program::select('category')->groupBy('category')->where('application', '0')->get();
        $categoriesApplications = Program::select('category')->groupBy('category')->where('application', '1')->get();

        $programsLast = Program::select('id', 'name', 'slug', 'text', 'icon', 'category', 'created_at')
        ->where('application', '0')
        ->orderByRaw('created_at DESC')
        ->limit(8)
        ->get();

        $applicationsLast = Program::select('id', 'name', 'slug', 'text', 'icon', 'category', 'created_at')
        ->where('application', '1')
        ->orderByRaw('created_at DESC')
        ->limit(8)
        ->get();

        $gamesHit = Game::select('id', 'name','slug', 'icon', 'date_out')
        ->orderByRaw('view DESC')
        ->limit(16)
        ->get();

        return(
            view('landing')
            ->with('programsHit', $programsHit)
            ->with('applicationsHit', $applicationsHit)
            ->with('categoriesPrograms', $categoriesPrograms)
            ->with('categoriesApplications', $categoriesApplications)
            ->with('programsLast', $programsLast)
            ->with('applicationsLast', $applicationsLast)
            ->with('gamesHit', $gamesHit)
            ->with('gamesHit5', $gamesHit5)
            ->with('programsFullHit', $programsFullHit)
        );
    }
}
