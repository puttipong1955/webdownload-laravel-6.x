<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Program;
use App\Game;
use Illuminate\Support\Facades\Input;


class ProgramsController extends Controller
{
    public function index (){
        $data = Program::select('id', 'name', 'slug', 'text', 'icon', 'category', 'created_at')
        ->where('application', '0')
        ->orderByRaw('created_at DESC')
        ->paginate(10);

        $programsHit = Program::select('id', 'name', 'slug', 'text', 'icon', 'category')
        ->where('application', '0')
        ->orderByRaw('download DESC')
        ->limit(5)
        ->get();

        $applicationsHit = Program::select('id', 'name', 'slug', 'text', 'icon', 'category')
        ->where('application', '1')
        ->orderByRaw('download DESC')
        ->limit(5)
        ->get();

        $gamesHit = Game::select('id', 'slug', 'icon', 'date_out')
        ->orderByRaw('view DESC')
        ->limit(6)
        ->get();

        $categories = Program::select('category')->groupBy('category')->where('application', '0')->get();

        $programsRandom = Program::select('id', 'name', 'slug', 'text', 'icon', 'category')
        ->where('application', '0')
        ->inRandomOrder()
        ->limit(10)
        ->get();

        return view('programs')
        ->with('data', $data)
        ->with('categories', $categories)
        ->with('programsHit', $programsHit)
        ->with('applicationsHit', $applicationsHit)
        ->with('gamesHit', $gamesHit)
        ->with('programsRandom', $programsRandom)
        ;
       }

       public function category ($category){

        $programsCategory = Program::select('id', 'name', 'slug', 'text', 'icon', 'category', 'created_at')
        ->where('application', '0')
        ->where('category', $category)
        ->orderByRaw('created_at DESC')
        ->get();

        $programsHit = Program::select('id', 'name', 'slug', 'text', 'icon', 'category')
        ->where('application', '0')
        ->orderByRaw('download DESC')
        ->limit(5)
        ->get();

        $applicationsHit = Program::select('id', 'name', 'slug', 'text', 'icon', 'category')
        ->where('application', '1')
        ->orderByRaw('download DESC')
        ->limit(5)
        ->get();

        $gamesHit = Game::select('id', 'slug', 'icon', 'date_out')
        ->orderByRaw('view DESC')
        ->limit(6)
        ->get();

        $categories = Program::select('category')->groupBy('category')->where('application', '0')->get();

        return view('programs_category')
        ->with('category', $category)
        ->with('programsCategory', $programsCategory)
        ->with('categories', $categories)
        ->with('programsHit', $programsHit)
        ->with('applicationsHit', $applicationsHit)
        ->with('gamesHit', $gamesHit)
        ;
       }

       public function programSlug ($slug){
        
        $program = Program::select('id', 'name', 'slug', 'text', 'icon', 'category', 'tag', 'size', 'os', 'copyright', 'version', 'news', 'link', 'pic', 'pic2', 'pic3', 'created_at')
        ->where('slug', $slug)
        ->first();
        if($program == null){
            return back();
        }

        $categories = Program::select('category')->groupBy('category')->where('application', '0')->get();

        $programsHit = Program::select('id', 'name', 'slug', 'text', 'icon', 'category')
        ->where('application', '0')
        ->orderByRaw('download DESC')
        ->limit(5)
        ->get();

        $applicationsHit = Program::select('id', 'name', 'slug', 'text', 'icon', 'category')
        ->where('application', '1')
        ->orderByRaw('download DESC')
        ->limit(5)
        ->get();

        $gamesHit = Game::select('id', 'slug', 'icon', 'date_out')
        ->orderByRaw('view DESC')
        ->limit(6)
        ->get();

        $programsRandomCategory = Program::select('id', 'name', 'slug', 'text', 'icon', 'category')
        ->where('application', '0')
        ->where('category', $program->category)
        ->inRandomOrder()
        ->limit(3)
        ->get();

        return view('program')
        ->with('program', $program)
        ->with('categories', $categories)
        ->with('programsHit', $programsHit)
        ->with('applicationsHit', $applicationsHit)
        ->with('gamesHit', $gamesHit)
        ->with('programsRandomCategory', $programsRandomCategory)
        ;
       }

       public function increment ($application, $id, $link){
        Program::where('id', $id)->where('application', $application)->increment('download', 1);
        
        if($application == 2){
            Game::where('id', $id)->where('application', $application)->increment('view', 1);
        }

        $replaced = str_replace(['~'], "/", $link);
        return Redirect::to($replaced);
       }

    public function search (Request $request){
        $word = $request->get('word');
        $programs = Program::select('id', 'name', 'slug', 'text', 'icon', 'category', 'application', 'created_at')
        ->where('name', 'like', '%'.$word.'%')
        ->orWhere('category', 'like', '%'.$word.'%');

        $data = Game::select('id', 'name', 'slug', 'info', 'icon', 'category', 'application', 'created_at')
        ->where('name', 'like', '%'.$word.'%')
        ->orWhere('category', 'like', '%'.$word.'%')
        ->union($programs)
        ->paginate(10)
        ->setPath ( '' );

        $data->appends ( array (
            'word' => $request->input ( 'word' ) 
          ));

        $programsHit = Program::select('id', 'name', 'slug', 'text', 'icon', 'category')
        ->where('application', '0')
        ->orderByRaw('download DESC')
        ->limit(5)
        ->get();

        $applicationsHit = Program::select('id', 'name', 'slug', 'text', 'icon', 'category')
        ->where('application', '1')
        ->orderByRaw('download DESC')
        ->limit(5)
        ->get();

        $gamesHit = Game::select('id', 'icon', 'slug', 'date_out')
        ->orderByRaw('view DESC')
        ->limit(6)
        ->get();

        return view('search')
        ->with('data', $data)
        ->with('programsHit', $programsHit)
        ->with('applicationsHit', $applicationsHit)
        ->with('gamesHit', $gamesHit)
        ->with('word', $word);
    }

    public function tag ($tag){
        $programs = Program::select('id', 'name', 'slug', 'text', 'icon', 'category', 'application', 'created_at')
        ->where('tag', 'like', '%'.$tag.'%');

        $data = Game::select('id', 'name', 'slug', 'info',  'icon', 'category', 'application', 'created_at')
        ->where('tag', 'like', '%'.$tag.'%')
        ->union($programs)
        ->paginate(10);

        $programsHit = Program::select('id', 'name', 'slug', 'text', 'icon', 'category')
        ->where('application', '0')
        ->orderByRaw('download DESC')
        ->limit(5)
        ->get();

        $applicationsHit = Program::select('id', 'name', 'slug', 'text', 'icon', 'category')
        ->where('application', '1')
        ->orderByRaw('download DESC')
        ->limit(5)
        ->get();

        $gamesHit = Game::select('id', 'slug', 'icon', 'date_out')
        ->orderByRaw('view DESC')
        ->limit(6)
        ->get();

        return view('tag')
        ->with('data', $data)
        ->with('programsHit', $programsHit)
        ->with('applicationsHit', $applicationsHit)
        ->with('gamesHit', $gamesHit)
        ->with('tag', $tag);
    }

    public function getProgramsCategoies()
    {
        $programsCategory = Program::where('application', 0)->select('category')->groupBy('category')->get();
        return $programsCategory;
    }
       
}
