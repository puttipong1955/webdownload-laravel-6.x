<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Program;
use App\Game;

class ApplicationsController extends Controller
{
    public function index (){
        $data = Program::select('id', 'name', 'slug', 'text', 'icon', 'category', 'created_at')
        ->where('application', '1')
        ->orderByRaw('created_at DESC')
        ->paginate(10);

        $programsHit = Program::select('id', 'name', 'slug', 'text', 'icon', 'category')
        ->where('application', '0')
        ->orderByRaw('download DESC')
        ->limit(5)
        ->get();

        $applicationsHit = Program::select('id', 'name', 'slug', 'text', 'icon', 'category')
        ->where('application', '1')
        ->orderByRaw('download DESC')
        ->limit(5)
        ->get();

        $gamesHit = Game::select('id', 'slug', 'icon', 'date_out')
        ->orderByRaw('view DESC')
        ->limit(6)
        ->get();

        $applicationsRandom = Program::select('id', 'name', 'slug', 'text', 'icon', 'category')
        ->where('application', '1')
        ->inRandomOrder()
        ->limit(10)
        ->get();

        $categories = Program::select('category')->groupBy('category')->where('application', '1')->get();
        return view('applications')
        ->with('data', $data)
        ->with('categories', $categories)
        ->with('programsHit', $programsHit)
        ->with('applicationsHit', $applicationsHit)
        ->with('gamesHit', $gamesHit)
        ->with('applicationsRandom', $applicationsRandom)
        ;
       }

    public function category ($category){

        $programsCategory = Program::select('id', 'name', 'slug', 'text', 'icon', 'category', 'created_at')
        ->where('application', '1')
        ->where('category', $category)
        ->orderByRaw('created_at DESC')
        ->get();

        $programsHit = Program::select('id', 'name', 'slug', 'text', 'icon', 'category')
        ->where('application', '0')
        ->orderByRaw('download DESC')
        ->limit(5)
        ->get();

        $applicationsHit = Program::select('id', 'name', 'slug', 'text', 'icon', 'category')
        ->where('application', '1')
        ->orderByRaw('download DESC')
        ->limit(5)
        ->get();

        $gamesHit = Game::select('id', 'slug', 'icon', 'date_out')
        ->orderByRaw('view DESC')
        ->limit(6)
        ->get();

        $categories = Program::select('category')->groupBy('category')->where('application', '1')->get();

        return view('applications_category')
        ->with('category', $category)
        ->with('programsCategory', $programsCategory)
        ->with('categories', $categories)
        ->with('programsHit', $programsHit)
        ->with('applicationsHit', $applicationsHit)
        ->with('gamesHit', $gamesHit)
        ;
       }

    public function applicationSlug ($slug){
        
        $app = Program::select('id', 'name', 'slug', 'text', 'icon', 'category', 'tag', 'size', 'os', 'copyright', 'version', 'news', 'link', 'pic', 'pic2', 'pic3', 'created_at')
        ->where('slug', $slug)
        ->first();

        if($app == null){
            return back();
        }

        $categories = Program::select('category')->groupBy('category')->where('application', '1')->get();

        $programsHit = Program::select('id', 'name', 'slug', 'text', 'icon', 'category')
        ->where('application', '0')
        ->orderByRaw('download DESC')
        ->limit(5)
        ->get();

        $applicationsHit = Program::select('id', 'name', 'slug', 'text', 'icon', 'category')
        ->where('application', '1')
        ->orderByRaw('download DESC')
        ->limit(5)
        ->get();

        $gamesHit = Game::select('id', 'slug', 'icon', 'date_out')
        ->orderByRaw('view DESC')
        ->limit(6)
        ->get();

        $appsRandomCategory = Program::select('id', 'name', 'slug', 'text', 'icon', 'category')
        ->where('application', '1')
        ->where('category', $app->category)
        ->inRandomOrder()
        ->limit(3)
        ->get();

        return view('application')
        ->with('app', $app)
        ->with('categories', $categories)
        ->with('programsHit', $programsHit)
        ->with('applicationsHit', $applicationsHit)
        ->with('gamesHit', $gamesHit)
        ->with('appsRandomCategory', $appsRandomCategory)
        ;
       }

    public function getApplicationsCategoies()
    {
        $applicationsCategory = Program::where('application', 1)->select('category')->groupBy('category')->get();
        return $applicationsCategory;
    }
}
