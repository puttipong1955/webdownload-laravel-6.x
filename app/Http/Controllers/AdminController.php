<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Program;
use App\Game;
use App\GameLink;
use DB;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $programViews = Program::select('download')->where('application', 0)->get();
        $appViews = Program::select('download')->where('application', 1)->get();
        $gameViews = Game::select('view')->get();

        $programsHit = Program::select('id', 'name', 'icon', 'download')
        ->where('application', '0')
        ->orderByRaw('download DESC')
        ->limit(5)
        ->get();
        $applicationsHit = Program::select('id', 'name', 'icon', 'download')
        ->where('application', '1')
        ->orderByRaw('download DESC')
        ->limit(5)
        ->get();
        $gamesHit = Game::select('id', 'name', 'icon', 'view')
        ->orderByRaw('view DESC')
        ->limit(5)
        ->get();

        return view('admin/home')
        ->with('programViews', $programViews)
        ->with('appViews', $appViews)
        ->with('gameViews', $gameViews)
        ->with('programsHit', $programsHit)
        ->with('applicationsHit', $applicationsHit)
        ->with('gamesHit', $gamesHit)
        ;
    }

    public function programs()
    {
        $programsLast = Program::select('id', 'name', 'slug', 'icon', 'download', 'category', 'created_at', 'application')
        ->where('application', '0')
        ->orderByRaw('created_at DESC')
        ->paginate(20);

        $categories = Program::select('category')->groupBy('category')->where('application', 0)->get();

        $programsHit = Program::select('id', 'name', 'icon', 'download','created_at')
        ->where('application', '0')
        ->orderByRaw('download DESC')
        ->limit(10)
        ->get();

        return view('admin/admin_programs')
        ->with('programsLast', $programsLast)
        ->with('categories', $categories)
        ->with('programsHit', $programsHit)
        ;
    }

    public function applications()
    {
        $applicationsLast = Program::select('id', 'name', 'slug', 'icon', 'download', 'category', 'created_at', 'application')
        ->where('application', '1')
        ->orderByRaw('created_at DESC')
        ->paginate(20);

        $categories = Program::select('category')->groupBy('category')->where('application', 1)->get();

        $applicationsHit = Program::select('id', 'name', 'icon', 'download','created_at')
        ->where('application', '1')
        ->orderByRaw('download DESC')
        ->limit(10)
        ->get();

        return view('admin/admin_applications')
        ->with('applicationsLast', $applicationsLast)
        ->with('categories', $categories)
        ->with('applicationsHit', $applicationsHit)
        ;
    }

    public function games()
    {
        $gamesLast = Game::select('id', 'name', 'icon', 'slug', 'view', 'category', 'created_at')
        ->orderByRaw('created_at DESC')
        ->paginate(20);

        $categories = Game::select('category')->groupBy('category')->get();

        $gamesHit = Game::select('id', 'name', 'icon', 'view','created_at')
        ->orderByRaw('view DESC')
        ->limit(10)
        ->get();

        return view('admin/admin_games')
        ->with('gamesLast', $gamesLast)
        ->with('categories', $categories)
        ->with('gamesHit', $gamesHit)
        ;
    }

    public function programsCategory($category)
    {
        $programsCategory = Program::select('id', 'name', 'icon', 'download', 'category', 'created_at')
        ->where('application', '0')
        ->where('category', $category)
        ->orderByRaw('created_at DESC')
        ->paginate(20);

        $categories = Program::select('category')->groupBy('category')->where('application', 0)->get();

        $programsHit = Program::select('id', 'name', 'icon', 'download','created_at')
        ->where('application', '0')
        ->orderByRaw('download DESC')
        ->limit(10)
        ->get();

        return view('admin/admin_programs_category')
        ->with('programsCategory', $programsCategory)
        ->with('categories', $categories)
        ->with('programsHit', $programsHit)
        ->with('category', $category)
        ;
    }

    public function applicationsCategory($category)
    {
        $applicationsCategory = Program::select('id', 'name', 'icon', 'download', 'category', 'created_at')
        ->where('application', '1')
        ->where('category', $category)
        ->orderByRaw('created_at DESC')
        ->paginate(20);

        $categories = Program::select('category')->groupBy('category')->where('application', 1)->get();

        $applicationsHit = Program::select('id', 'name', 'icon', 'download','created_at')
        ->where('application', '1')
        ->orderByRaw('download DESC')
        ->limit(10)
        ->get();

        return view('admin/admin_applications_category')
        ->with('applicationsCategory', $applicationsCategory)
        ->with('categories', $categories)
        ->with('applicationsHit', $applicationsHit)
        ->with('category', $category)
        ;
    }

    public function gamesCategory($category)
    {
        $gamesCategory = Game::select('id', 'name', 'icon', 'view', 'category', 'created_at')
        ->where('category', $category)
        ->orderByRaw('created_at DESC')
        ->paginate(20);

        $categories = Game::select('category')->groupBy('category')->get();

        $gamesHit = Game::select('id', 'name', 'icon', 'view','created_at')
        ->orderByRaw('view DESC')
        ->limit(10)
        ->get();

        return view('admin/admin_games_category')
        ->with('gamesCategory', $gamesCategory)
        ->with('categories', $categories)
        ->with('gamesHit', $gamesHit)
        ->with('category', $category)
        ;
    }

    public function search (Request $request){
        $word = $request->get('word');
        $programs = Program::select('id', 'name', 'icon', 'category', 'application', 'created_at', 'download')
        ->where('name', 'like', '%'.$word.'%')
        ->orWhere('category', 'like', '%'.$word.'%');

        $data = Game::select('id', 'name', 'icon', 'category', 'application', 'created_at', 'view')
        ->where('name', 'like', '%'.$word.'%')
        ->orWhere('category', 'like', '%'.$word.'%')
        ->union($programs)
        ->paginate(20)
        ->setPath ( '' );

        $data->appends ( array (
            'word' => $request->input ( 'word' ) 
          ));

        return view('admin/admin_search')
        ->with('data', $data)
        ->with('word', $word);
    }

    public function programDel($application, $id)
    {
        if($application == 2){
            $game = Game::findorfail($id);
            $game->delete();  
            $gameLink = GameLink::where('id_game', $id);
            $gameLink->delete(); 
        }else{
            $program = Program::findorfail($id);
            $program->delete();  
        }
        
        if($application == 0){
            return Redirect::to('/admin/programs'); 
        }else if($application == 1){
            return Redirect::to('/admin/applications');
        }else{
            return Redirect::to('/admin/games');
        }   
    }

    public function programStore(Request $request)
    {
        $request->validate([
            'name'=>'required',
            'text'=>'required',
            'tag'=>'required',
            'icon'=>'required',
            'link'=>'required',
            'pic'=>'required']);

        $replaced = str_replace(" ", "-", $request->input('name'));

        $program = new Program;

        $program->name = $request->input('name');
        $program->slug = $replaced;
        $program->text = $request->input('text');
        $program->tag = $request->input('tag');
        $program->news = $request->input('news');
        $program->category = $request->input('category');
        $program->os = $request->input('os');
        $program->copyright = $request->input('copyright');
        $program->size = $request->input('size');
        $program->version = $request->input('version');
        $program->icon = $request->input('icon');
        $program->link = $request->input('link');
        $program->pic = $request->input('pic');
        $program->pic2 = $request->input('pic2');
        $program->pic3 = $request->input('pic3');

        $program->save();
        
        return redirect('/admin/programs')->with('success', 'Data Saved');
    }

    public function applicationStore(Request $request)
    {
        $request->validate([
            'name'=>'required',
            'text'=>'required',
            'tag'=>'required',
            'icon'=>'required',
            'link'=>'required',
            'pic'=>'required']);

        $replaced = str_replace(" ", "-", $request->input('name'));

        $application = new Program;

        $application->name = $request->input('name');
        $application->slug = $replaced;
        $application->text = $request->input('text');
        $application->tag = $request->input('tag');
        $application->news = $request->input('news');
        $application->category = $request->input('category');
        $application->os = $request->input('os');
        $application->copyright = $request->input('copyright');
        $application->size = $request->input('size');
        $application->version = $request->input('version');
        $application->icon = $request->input('icon');
        $application->link = $request->input('link');
        $application->pic = $request->input('pic');
        $application->pic2 = $request->input('pic2');
        $application->pic3 = $request->input('pic3');
        $application->application = '1';

        $application->save();
        
        return redirect('/admin/applications')->with('success', 'Data Saved');
    }

    public function gameStore(Request $request)
    {
        $request->validate([
            'name'=>'required',
            'text'=>'required',
            'tag'=>'required',
            'icon'=>'required',
            'video'=>'required',
            'pic'=>'required']);

        $replaced = str_replace(" ", "-", $request->input('name'));

        $game = new Game;

        $game->name = $request->input('name');
        $game->slug = $replaced;
        $game->info = $request->input('text');
        $game->tag = $request->input('tag');
        $game->company = $request->input('company');
        $game->category = $request->input('category');
        $game->os = $request->input('os');
        $game->date_out = $request->input('date_out');
        $game->size = $request->input('size');
        $game->cpu = $request->input('cpu');
        $game->icon = $request->input('icon');
        $game->ram = $request->input('ram');
        $game->disk = $request->input('disk');
        $game->grafig = $request->input('grafig');
        $game->video = $request->input('video');
        $game->pic = $request->input('pic');
        $game->pic2 = $request->input('pic2');
        $game->pic3 = $request->input('pic3');
        $game->pic4 = $request->input('pic4');
        $game->application = '2';

        $game->save();
        
        return redirect('/admin/games')->with('success', 'Data Saved');
    }

    public function gameLinkStore(Request $request)
    {
        $request->validate([
            'form-game-link'=>'required',
            'form-game-part'=>'required',
            'form-game-type'=>'required']);

        $gameLink = new GameLink;

        $gameLink->id_game = $request->input('game-id');
        $gameLink->link = $request->input('form-game-link');
        $gameLink->part = $request->input('form-game-part');
        $gameLink->type = $request->input('form-game-type');

        $gameLink->save();
        return back();
        
    }

    public function search2 ($idd){
        $program = Program::where('id', $idd)->first();
        return response()
        ->json(['id' => $program['id'],
        'name' => $program['name'],
        'text' => $program['text'],
        'news' => $program['news'],
        'tag' => $program['tag'],
        'category' => $program['category'],
        'os' => $program['os'],
        'copyright' => $program['copyright'],
        'size' => $program['size'],
        'version' => $program['version'],
        'link' => $program['link'],
        'icon' => $program['icon'],
        'pic' => $program['pic'],
        'pic2' => $program['pic2'],
        'pic3' => $program['pic3'],]);
    }

    public function programPut(Request $request)
    {
        $request->validate([
            'name'=>'required',
            'text'=>'required',
            'tag'=>'required',
            'icon'=>'required',
            'link'=>'required',
            'pic'=>'required']);

            $replaced = str_replace(" ", "-", $request->input('name'));
            
            $id = $request->input('id');
            $name = $request->input('name');
            $text = $request->input('text');
            $tag = $request->input('tag');
            $news = $request->input('news');
            $category = $request->input('category');
            $os = $request->input('os');
            $copyright = $request->input('copyright');
            $size = $request->input('size');
            $version = $request->input('version');
            $icon = $request->input('icon');
            $link = $request->input('link');
            $pic = $request->input('pic');
            $pic2 = $request->input('pic2');
            $pic3 = $request->input('pic3');
    
            DB::table('programs')
            ->where('id',$id)
            ->update([
                'name'=>$name,
                'slug'=>$replaced,
                'text'=>$text,
                'tag'=>$tag,
                'news'=>$news,
                'category'=>$category,
                'os'=>$os, 
                'copyright'=>$copyright,
                'size'=>$size,
                'version'=>$version,
                'icon'=>$icon,
                'link'=>$link,
                'pic'=>$pic,
                'pic2'=>$pic2,
                'pic3'=>$pic3,
                ]);
       
        return redirect('/admin/programs')->with('success', 'Data Saved');
    }

    public function applicationPut(Request $request)
    {
        $request->validate([
            'name'=>'required',
            'text'=>'required',
            'tag'=>'required',
            'icon'=>'required',
            'link'=>'required',
            'pic'=>'required']);

            $replaced = str_replace(" ", "-", $request->input('name'));
            
            $id = $request->input('id');
            $name = $request->input('name');
            $text = $request->input('text');
            $tag = $request->input('tag');
            $news = $request->input('news');
            $category = $request->input('category');
            $os = $request->input('os');
            $copyright = $request->input('copyright');
            $size = $request->input('size');
            $version = $request->input('version');
            $icon = $request->input('icon');
            $link = $request->input('link');
            $pic = $request->input('pic');
            $pic2 = $request->input('pic2');
            $pic3 = $request->input('pic3');
    
            DB::table('programs')
            ->where('id',$id)
            ->update([
                'name'=>$name,
                'slug'=>$replaced,
                'text'=>$text,
                'tag'=>$tag,
                'news'=>$news,
                'category'=>$category,
                'os'=>$os, 
                'copyright'=>$copyright,
                'size'=>$size,
                'version'=>$version,
                'icon'=>$icon,
                'link'=>$link,
                'pic'=>$pic,
                'pic2'=>$pic2,
                'pic3'=>$pic3,
                ]);
       
        return redirect('/admin/applications')->with('success', 'Data Saved');
    }

    public function getGame ($idgame){
        $game = Game::where('id', $idgame)->first();
        return response()
        ->json(['id' => $game['id'],
        'name' => $game['name'],
        'info' => $game['info'],
        'company' => $game['company'],
        'tag' => $game['tag'],
        'category' => $game['category'],
        'os' => $game['os'],
        'date_out' => $game['date_out'],
        'cpu' => $game['cpu'],
        'ram' => $game['ram'],
        'grafig' => $game['grafig'],
        'disk' => $game['disk'],
        'size' => $game['size'],
        'video' => $game['video'],
        'icon' => $game['icon'],
        'pic' => $game['pic'],
        'pic2' => $game['pic2'],
        'pic3' => $game['pic3'],
        'pic4' => $game['pic4'],]);
    }

    public function gamePut(Request $request)
    {
        $request->validate([
            'name'=>'required',
            'text'=>'required',
            'tag'=>'required',
            'icon'=>'required',
            'video'=>'required',
            'pic'=>'required']);
            
            $replaced = str_replace(" ", "-", $request->input('name'));
            
            $id = $request->input('id');
            $name = $request->input('name');
            $info = $request->input('text');
            $tag = $request->input('tag');
            $company = $request->input('company');
            $category = $request->input('category');
            $os = $request->input('os');
            $date_out = $request->input('date_out');
            $size = $request->input('size');
            $cpu = $request->input('cpu');
            $ram = $request->input('ram');
            $grafig = $request->input('grafig');
            $disk = $request->input('disk');
            $pic4 = $request->input('pic4');
            $icon = $request->input('icon');
            $video = $request->input('video');
            $pic = $request->input('pic');
            $pic2 = $request->input('pic2');
            $pic3 = $request->input('pic3');
    
            DB::table('games')
            ->where('id',$id)
            ->update([
                'name'=>$name,
                'slug'=>$replaced,
                'info'=>$info,
                'tag'=>$tag,
                'company'=>$company,
                'category'=>$category,
                'os'=>$os, 
                'date_out'=>$date_out,
                'size'=>$size,
                'cpu'=>$cpu,
                'ram'=>$ram,
                'disk'=>$disk,
                'grafig'=>$grafig,
                'pic4'=>$pic4,
                'icon'=>$icon,
                'video'=>$video,
                'pic'=>$pic,
                'pic2'=>$pic2,
                'pic3'=>$pic3,
                ]);
       
        return redirect('/admin/games')->with('success', 'Data Saved');
    }

    public function gameLinkPut(Request $request)
    {
        $request->validate([
            'form-game-link'=>'required',
            'form-game-part'=>'required',
            'form-game-type'=>'required']);
        
        $id = $request->input('id-link');
        $id_game = $request->input('game-id');
        $link = $request->input('form-game-link');
        $part = $request->input('form-game-part');
        $type = $request->input('form-game-type');

        DB::table('game_links')
            ->where('id',$id)
            ->update([
                'id_game'=>$id_game,
                'link'=>$link,
                'part'=>$part,
                'type'=>$type
                ]);
        return back();
        
    }

    public function getGameLinks ($idgame){
        $gameLinks = GameLink::where('id_game', $idgame)->orderBy('type', 'desc')->get();
        return [$gameLinks, $idgame];
    }

    public function getGameLink ($id){
        $gameLinks = GameLink::where('id', $id)->first();
        return $gameLinks;
    }

    public function gameLinkDel($id)
    {
        $gameLink = GameLink::findorfail($id);
        $gameLink->delete(); 

        return back();
    }

    public function getProgramsCategoies()
    {
        $programsCategory = Program::where('application', 0)->select('category')->groupBy('category')->get();
        return $programsCategory;
    }
    public function getApplicationsCategoies()
    {
        $applicationsCategory = Program::where('application', 1)->select('category')->groupBy('category')->get();
        return $applicationsCategory;
    }
    public function getGamesCategoies()
    {
        $gamesCategory = Game::select('category')->groupBy('category')->get();
        return $gamesCategory;
    }
    public function test()
    {
        return view('admin/test');
    }
}
