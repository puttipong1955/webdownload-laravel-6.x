<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Game;
use App\GameLink;

class GamesController extends Controller
{
    public function index(){

        $gamesHit = Game::select('id', 'name', 'slug', 'icon', 'date_out')
        ->orderByRaw('view DESC')
        ->limit(15)
        ->get();

        $game1 = Game::select('id', 'video', 'name', 'slug', 'icon', 'date_out')
        ->orderByRaw('view DESC')
        ->first();

        $gamesLast = Game::select('id', 'name', 'slug', 'icon', 'date_out')
        ->orderByRaw('created_at DESC')
        ->paginate(15);

        return view('games')
        ->with('gamesHit', $gamesHit)
        ->with('game1', $game1)
        ->with('gamesLast', $gamesLast)
        ;
    }

    public function category ($category){

        $gamesCategory = Game::select('id', 'name', 'icon', 'date_out')
        ->where('application', '2')
        ->where('category', $category)
        ->orderByRaw('created_at DESC')
        ->get();

        return view('games_category')
        ->with('gamesCategory', $gamesCategory)
        ->with('category', $category);
       }

    public function gameSlug ($slug){
        
        $game = Game::where('slug', $slug)->first();
        $gameLinks = GameLink::where('id_game', $game->id)->orderBy('type', 'desc')->get();

        if($game == null){
            return back();
        }

        return view('game')
        ->with('game', $game)
        ->with('gameLinks', $gameLinks)
        ;
       }

    public function getGamesCategoies()
       {
           $gamesCategory = Game::select('category')->groupBy('category')->get();
           return $gamesCategory;
       }
}
